function longitud() {
	var numero = parseInt(document.getElementById('numero').value)// Almaceno el valor del numero intorducido por el usuario
	var parrafo = document.getElementById('parrafo')// Almaceno la referencia al parrafo de HTML
	var longitud = 10// Declaro una variable que emnpieza por el 10, sera el ambito que utilizare para saber las cifras del numero
	var cantidad = 1// Un contador para las cifras del numero

	/*Mediante un bucle while compruebo si el numero es menor o igual que 10, si es asi, significa que tiene 2 o mas cifras, 
	  por lo que multilpico longitud por 10 e incremento el contador, asi sucesivamente hasta que el numero sea menor que longitud.
	  En el caso de ser el numero menor que 10 significaria que tiene 1 cifra, por eso cantidad esta inicializada a 1
	  */

	while(numero >= longitud){
		longitud *= 10
		cantidad++
	}
	
	parrafo.innerHTML = "La longitud del numero: " + numero + " es: " + cantidad// Muestro la información en el cuerpo HTML
}