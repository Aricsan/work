function dia_sig() {
	var fecha = new Date(document.getElementById('fecha').value)// Almaceno la fecha introducida por el usuario
	var parrafo = document.getElementById('parrafo')// Almaceno la referencia al parrafo de HTML
	
	fecha.setDate(fecha.getDate() + 1)// Con la función setDate cambio el dia del mes, para ello "saco" mediante getDate el dia del mes de la fecha introducida por el usuario y le sumo 1
	var dia = fecha.getDate()// Almaceno el dia de la nueva fecha
	var mes = fecha.getMonth()+1// Almaceno el mes de la nueva fecha, le sumo 1 porque empieza a contar por 0
	var año = fecha.getFullYear()// Almaceno el año entero de la nueva fecha
	
	if (isNaN(mes, año, dia))// Compruebo si la fecha introducida es correcta
		parrafo.innerHTML = "La fecha introducida es incorrecta" 
	else
		parrafo.innerHTML = "La fecha con un dia mas es: " + dia + " / " + mes + " / " + año// Mustro la información en el cuerpo HTML
}