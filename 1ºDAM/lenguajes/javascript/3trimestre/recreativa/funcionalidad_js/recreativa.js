// Variables globales, que almacenan la información de la puntuación y guardar
var puntaje = 0
var guardar = 0
var veces_guar = 0

// Funciones encargadas de hacer aparecer a los diferentes elementos HTML
function juego() {
	var bloque = document.getElementById('recreativa').style.display = 'block'
}

function inicio() {
	var bloque = document.getElementById('normas').style.display = 'block'
}

function acerca_de() {
	var bloque = document.getElementById('acerca_de').style.display = 'block'
}

// Función encargada de mostrar 3 imagenes aleatorias
function jugar() {	
	var fresa = document.getElementById('fresa')
	var limon = document.getElementById('limon')
	var sandia = document.getElementById('sandia')
	var imagenes = ["../imagenes/fresa.png", "../imagenes/limon.png", "../imagenes/sandia.png"]				// La dirección de las imagenes estan almacenadas en un array

	fresa.src = imagenes[parseInt(Math.random()*3)]															// Mediante la propiedad src cambio la dirección de las imagenes de forma aleatoria utilizando el array
	limon.src = imagenes[parseInt(Math.random()*3)]
	sandia.src = imagenes[parseInt(Math.random()*3)]

	puntuacion (fresa, limon, sandia)																		// Al final se llama a la función puntuación para comprobar la puntuación obtenida
	veces_guar = 0																							// Variable que comprueba la cantidad de veces que se guarda la puntuación
}

// Función encargada de cambiar la imagen correspondiente
function cambiar(boton) {																					// La funcion recibe como parametro this, este parametro cambia dependiendo de como es llamada la función
	var camb = boton.id 																					// Mediante el metodo id se obtiene el id del elemento que llama a la función
	var imagenes = ["../imagenes/fresa.png", "../imagenes/limon.png", "../imagenes/sandia.png"]
	var fresa = document.getElementById('fresa')
	var limon = document.getElementById('limon')
	var sandia = document.getElementById('sandia')

	// Dependiendo del id del elemento HTML cambio de forma aleatoria una imagen u otra
	if (camb == 'camb_1')
		fresa.src = imagenes[parseInt(Math.random()*3)]
	else if (camb == 'camb_2')
			limon.src = imagenes[parseInt(Math.random()*3)]
		else if (camb == 'camb_3')
			sandia.src = imagenes[parseInt(Math.random()*3)]

	puntuacion (fresa, limon, sandia)
	veces_guar = 0
}

// Función encargada de comprobar la puntuación en cada tirada
function puntuacion(fresa, limon, sandia) {																	// La función recibe como parametros las tres imagenes del juego
	var tir_act = document.getElementById('tir_actual')
	
	// Se comproeba las imagenes y se asigna la puntuación correspondiente
	if (fresa.src == limon.src && fresa.src == sandia.src)
		puntaje += 1000
	else if (fresa.src == limon.src || fresa.src == sandia.src ||
			limon.src == sandia.src)
			puntaje += 500
	
	tir_act.innerHTML = puntaje
}

// Función encargada de reiniciar o guardar una tirada
function rei_guar(boton) {																					// Al igual que cambiar, esta función recibe como parametro this
	var eleccion = boton.id 																				// Mediante e metodo id obtenemos el id del elemento HTML utilizado para llamas a la función
	var marc_act = document.getElementById('marc_actual')
	var tir_act = document.getElementById('tir_actual')
	veces_guar++																							// Incrementamos la variable veces_guar para que solo se pueda guardar una vez por tirada
	
	// Se comprueba el id del elemento HTML utilizado para llamar a la función junto
	// con la cantidad de veces que se guarda, dependiendo de ello se reinicia o guarda la puntuación
	if (eleccion == 'guardar' && veces_guar == 1)
		guardar += puntaje
	else if (eleccion == 'reiniciar'){
		puntaje = 0
		guardar = 0
	}
		
	tir_act.innerHTML = puntaje
	marc_act.innerHTML = guardar
}