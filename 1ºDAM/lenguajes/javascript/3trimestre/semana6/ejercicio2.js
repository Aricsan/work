function potencia() {
	var base = parseInt(document.getElementById('base').value)
	var exponente = parseInt(document.getElementById('exponente').value)
	var parrafo = document.getElementById('parrafo')
	var numero = 1

	// Comprobamos que el exponente introducido sea positivo
	if (exponente >= 0){
	// Calculamos la potencia multiplicando el numero cada vuelta del bucle por el exponente
	for (var i = 0; i < exponente; i++)
		numero *= base
	// Mostramos el resultado
	parrafo.innerHTML = base + " ^ " + exponente + " = " + numero
	}
	// En caso contrario mostramos mensaje de error
	else
		parrafo.innerHTML = "El exponente debe de ser un numero positivo"
}