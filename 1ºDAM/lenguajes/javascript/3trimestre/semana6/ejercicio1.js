function linea_cifras() {
	var numero = parseInt(document.getElementById('numero').value)
	var parrafo = document.getElementById('parrafo')
	var longitud = 10
	var cifras = 1
	var num_dividido

	// Calcular el numero de cifras 
	while(numero >= longitud){
		longitud *= 10
		cifras++
	}
	longitud/=10// da una vuelta mas...
	
	parrafo.innerHTML = ""
	// imprimir cada cifra en una line
	for (var i = 0; i < cifras; i++, longitud/=10) {
		num_dividido = Math.trunc(numero / longitud)// utilizo ula función Math.trunc() para quedarme con el cociente, el numero sin decimales
		parrafo.innerHTML += "Cifra " + i + " es " + num_dividido % 10 + "<br>"//Mediante el modulo obtengo siempre la ultima cifra como resto
	}
	
}