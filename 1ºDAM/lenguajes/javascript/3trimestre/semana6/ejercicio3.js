function inverso() {
	var numero = parseInt(document.getElementById('numero').value)
	var parrafo = document.getElementById('parrafo')
	var longitud = 10
	var cifras = 1
	var num_inverso = 0

	// Calcular el numero de cifras 
	while(numero >= longitud){
		longitud *= 10
		cifras++
	}
	
	// Calcular inverso
	for (var i = 0; i < cifras; i++, num_inverso *= 10) {// Multiplicamos el num_inverso por 10 para hacer "hueco" al siguiente
		num_inverso += numero % 10//Le sumamos a num_inverso la ultima cifra del numero mediante el modulo
		numero = Math.trunc(numero /= 10)// Divido por 10 y utilizo ula función Math.trunc() para quedarme con el cociente, el numero sin decimales
		parrafo.innerHTML = "Numero inverso: " + num_inverso
	}
	
}