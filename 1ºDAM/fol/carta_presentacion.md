Introduccion  
Objetivo  
Caracteristicas  
Tipos  
Esquema/Contenido  
Algunos consejos mas  
Mis Cartas  

# Introduccion
    - C.V = Producto  
    - C.P = Envoltorio  
    - Intrumentos/Herramientas  

# Objetivos
    - Atencion  
    - Interes  
        - 1ºParrafo  
    - Deseo  
        - 2ºParrafo  
    - Accion  
        - 3ºParrafo  

# Caracteristicas
    - Presentacion Atractiva
    - Mismo tipo calidad y formato  
    - Nunca manuscrita excepto indicacion (nunca a mano)  
    - Estilo breve clave y conciso  
    - No es una version resumida del curriculum, si no, consiste en destacar los puntos fuertes, aquello que nos diferencia del resto, ya sea de nuestra vida profesional academinca o personal  
    - Siempre personalizada, dirigida a una persona en concreto y adaptada a una empresa  

# Tipos de C.P
    - Tipo contestacion anuncio  
    - Candidatura expontanea, empresa de selección o consultora  
    - candidatura expontanea a una empresa de informatica  

# Esquema/Contenido
    - Arriba a la derecha (menbrete, tus datos personales)  
    - Segundo parrafo arriba a la izquierda (destinatario)  
    - fecha entera a mano  
    - Despedida  
    - Firma  
