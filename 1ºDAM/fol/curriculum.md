Introdicción  
Los diez principios basicos  
Tres formas diferentes de estructurar mi curriculum  
El curriculum paso a paso  
Errores que debes evitar  
Mi curriculum  

# Introducción
- Importancia del curriculum
    - Porque es un instrumento de marketing  
    - Conseguir entrevista  
    - Imagen positiva/negativa  
    - Objetivo: abrirnos puertas  
- En que consiste un buen curriculum  
    - Claro  
    - Conciso  
    - Completo  

# Los diez principios basicos
- Completo  
- Breve y conciso  
- Siempre a ordenador  
- Color: depende de la moda, no demasiado  
- Lenguaje formal  
- Idioma español  
- En primera persona siempre   
- ¿Fotocopia? no  
- ¿Anexos? no  
- ¿Fotografia? si  

#Entrevista por Competencia
- Estamos actualmente en entornos muy cambiantes, por lo que se hace necesario evaluar la potencialidad del candidato para otros posibles puestos  
- Cada empresa tiene definidas una competencias (habilidades, cualidades) pero todas conindicen en las mismas para el primer empleo:
    - Entusiasmo y ganas de trabajar  
    - Capacidad de adaptación a la filosofia de la empresa  
    - Orientación al cliente, interno y externo  
    - Empatia  
    - Capacidad de aprendizaje  
    - Flexibilidad para adaptarse a los cambios  
    - Trabajo en equipo  
