\n = salto de linea
" \'texto\' " = comillas simples
" \"texto\" " = comillas dobles

.charAt(indice) = Devuelve un carácter de un texto dependiendo de lo que le insertemos en el indice.
.toUpperCase() = Cambia un texto a mayusculas
.toLowerCase() = Cambia un texto a minusculas
.replace(/a/g, "b") = reemplaza todas las letras a del texto por la letra b. El parametro g es para indicarle que lo haga con todo el texto porque sino lo haria solo con la primera palabra.
.includes("@") = compruba si en el texto hay lo introducido en el indice, devuelve true o false de si tiene o no tiene.
Math.floor() = Redondea un número hacia abajo.
Math.random()*10 +1= devuelve un numero aleatorio, en este caso entre 1 y 10. Escribimos +1 porque sino devolveria solo los numero comprendidos entre 0 y 9.
setInterval(esegundos, 10) = ejecuta una funcion cada x milisegundos, en este caso ejecuta la funcion esegundos cada 10 milisegundos
clearInterval(variable asociada a setInterval()) = detiene de forma indefinida la funcion setInterval().
parseInt(variable) = devuelve un numero entero de la variable especificada.
isNaN(variable) = intenta convertir la variable pasada a un número. Si no se puede convertir, devuelve true; en caso contrario, devuelve false

document.body.style.background="#FF0000" = cambiar el color de fondo.
document.getElementById("id").value = devuelve el valor de un elemento.
document.getElementById("id").checked = devuelve true o false dependiendode como este el elemento
document.getElementById("id").style.display = "inline o none o block" = posicionar un objeto desde javascript.

/*for (var i = 0; i < select.length; i++) {
		if (select.options[i].text == "Seleccione su edad") {
			select.selectedIndex = i;
		}
	}*/