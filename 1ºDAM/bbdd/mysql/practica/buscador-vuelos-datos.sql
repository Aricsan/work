
-- Fichero de Inserción de Registros

INSERT INTO COMPAÑIA VALUES
    ('NW','Northwest Airlines Inc.',NULL),
    ('MX','Trans World Airlines Inc.',NULL),
    ('PG','Air Canada',NULL),
    ('HC','United Airlines, Inc.',NULL),
    ('TE','Hiberia Airlines Inc.',NULL);
    
INSERT INTO AEROPUERTO VALUES 
    ('LEMD','Barajas_Airport','Madrid','España'),
    ('LATI','Tirana Airport','Tirana','Albania'),
    ('LOWG','Graz Airport','Graz','Austria'),
    ('EBBR','Bruselas Airport','Bruselas','Belgica'),
    ('UMMS','Minsk Airport','Minsk','Bielorrusia');

INSERT INTO TERMINALES VALUES
    ('4','EBBR'),
    ('2','LATI'),
    ('6','UMMS'),
    ('1','LOWG'),
    ('3','LEMD');

INSERT INTO VUELO VALUES 
    ('BA2467','NW','EBBR','LATI','En hora'),
    ('NC2174','MX','LATI','LATI','Cancelado'),
    ('PC9456','PG','UMMS','LOWG','Salida'),
    ('FG7241','HC','LEMD','UMMS','Retrasado'),
    ('PL9568','TE','LEMD','LOWG','Llegada');

INSERT INTO ASIENTOS VALUES 
    ('A2','Turista'),
    ('B3','Turista Premium'),
    ('C1','Business Class'),
    ('B6','Turista'),
    ('C8','Business Class');

INSERT INTO PASAJEROS VALUES
    ('X2315648L','Alejandro','Garcia','Noblejas'),
    ('P9546523U','Carla','Lopez','Sanchez'),
    ('X6451234N','Marius','Tonciu',NULL),
    ('C7894561I','Javier','Maroto','Perez'),
    ('Z4678451K','Florentino','Perez','Ortega');

INSERT INTO RESERVA VALUES
    ('PHJ754','P9546523U',345.21),
    ('FHT451','Z4678451K',120.48),
    ('LKO741','X2315648L',99),
    ('KJG453','C7894561I',750.65),
    ('FQS724','X6451234N',1489.26);

INSERT INTO RESERVA_VUELOS VALUES
    ('LKO741','BA2467'),
    ('FQS724','PL9568'),
    ('FHT451','NC2174'),
    ('FQS724','PL9568'),
    ('PHJ754','FG7241');

UPDATE AEROPUERTO SET OACI = 'PEND' WHERE OACI = 'LEMD'; 
UPDATE COMPAÑIA SET Nombre = 'Air EEUU' WHERE CodigoCOMPAÑIA = 'PG';
UPDATE COMPAÑIA SET CodigoCOMPAÑIA = 'AE' WHERE CodigoCOMPAÑIA = 'PG';
UPDATE RESERVA SET Localizador = 'PTG193' WHERE Localizador = 'PHJ754';
UPDATE VUELO SET CodigoVuelo = 'TR5714' WHERE CodigoVuelo = 'PL9568';

DELETE FROM RESERVA WHERE Localizador = 'FHT451';
DELETE FROM AEROPUERTO WHERE OACI = 'LATI';
DELETE FROM COMPAÑIA WHERE Nombre = 'Air EEUU';
DELETE FROM RESERVA WHERE Precio = 99.00;
DELETE FROM ASIENTOS WHERE CodigoAsiento = 'C8';
