
-- Creación de la Base de Datos junto con las tablas y sus campos

DROP DATABASE IF EXISTS buscadorvuelos;
CREATE DATABASE IF NOT EXISTS buscadorvuelos;
USE buscadorvuelos;

CREATE TABLE AEROPUERTO (
    CodIATA CHAR(3),
    Nombre VARCHAR(20),
    Ciudad VARCHAR(20),
    Pais VARCHAR(20)
);

CREATE TABLE TERMINALES (
    Numero CHAR(2),
    CodIATA CHAR(3)
);

CREATE TABLE VUELO (
    CodVuelo CHAR(7),
    CodCompañia CHAR(2),
    AeropuertoOrigen CHAR(4),
    AeropuertoDestino CHAR(4),
    Estado ENUM('Cancelado','Retrasado','En hora','Llegada','Salida')
);

CREATE TABLE ASIENTOS (
    CodAsiento CHAR(2),
    TipoClase ENUM('Turista','Turista Premium','Business Class')
);

CREATE TABLE PASAJEROS (
    DNI CHAR(9),
    Nombre VARCHAR(20),
    Apellido1 VARCHAR(20),
    Apellido2 VARCHAR(20)
);

CREATE TABLE RESERVA (
    Localizador CHAR(6),
    DNI CHAR(9),
    Precio DECIMAL(6,2)
);

CREATE TABLE RESERVA_VUELOS (
    Localizador CHAR(6),
    CodVuelo CHAR(7)
);

CREATE TABLE COMPAÑIA (
    CodCompañia CHAR(2),
    Nombre VARCHAR(30),
    Logo BLOB
);

SOURCE buscador-vuelos-modi.sql;
SOURCE buscador-vuelos-datos.sql;
SOURCE buscador-vuelos-consultas.sql;
