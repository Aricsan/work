DROP DATABASE IF EXISTS ejercicio1p;
CREATE DATABASE ejercicio1p;
USE ejercicio1p;
CREATE TABLE PERSONA (
    nombre VARCHAR(20),
    apellidos VARCHAR(20),
    trabajo VARCHAR(10),
    PRIMARY KEY (nombre, apellidos));

CREATE TABLE SITUACION (
    hora INT,
    lugar VARCHAR(20),
    vestuario VARCHAR(20),
    mercancia VARCHAR(20),
    nombrePERSONA VARCHAR(20),
    apellidosPERSONA VARCHAR(20),
    PRIMARY KEY (hora, lugar),
    FOREIGN KEY (nombrePERSONA, apellidosPERSONA) REFERENCES PERSONA(nombre, apellidos));

CREATE TABLE OBJETO (
    nombre VARCHAR(20),
    tamaño VARCHAR(10),
    PRIMARY KEY (nombre));

CREATE TABLE ESTA (
    horaSITUACION INT,
    lugarSITUACION VARCHAR(20),
    nombreOBJETO VARCHAR(20),
    FOREIGN KEY (horaSITUACION, lugarSITUACION) REFERENCES SITUACION(hora, lugar),
    FOREIGN KEY (nombreOBJETO) REFERENCES OBJETO(nombre));
