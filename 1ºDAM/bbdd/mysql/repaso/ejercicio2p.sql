DROP DATABASE IF EXISTS ejercicio2p;
CREATE DATABASE ejercicio2p;
USE ejercicio2p;
CREATE TABLE SPRINT (
    numero INT,
    limite VARCHAR(10),
    sin_empezar VARCHAR(20),
    realizandose VARCHAR(10),
    en_revision VARCHAR(10),
    historias VARCHAR(20),
    inicio VARCHAR(10),
    PRIMARY KEY (numero));

CREATE TABLE ROL (
    nombre VARCHAR(20),
    dueño VARCHAR(20),
    scrumaster VARCHAR(20),
    numeroSPRINT INT,
    FOREIGN KEY (numeroSPRINT) REFERENCES SPRINT(numero));

CREATE TABLE PILA (
    estado VARCHAR(10),
    descripcion VARCHAR(50),
    numeroSPRINT INT,
    FOREIGN KEY (numeroSPRINT) REFERENCES SPRINT(numero));


CREATE TABLE PRODUCTO (
    mejoras VARCHAR(20),
    numeroSPRINT INT,
    FOREIGN KEY (numeroSPRINT) REFERENCES SPRINT(numero));


