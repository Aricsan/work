DROP DATABASE IF EXISTS ejercicio7p;
CREATE DATABASE ejercicio7p;
USE ejercicio7p;
CREATE TABLE RELIGION (
    nombre VARCHAR(20),
    PRIMARY KEY (nombre));

CREATE TABLE FAMILIA (
    heraldica VARCHAR(20),
    nombre VARCHAR(20),
    zona VARCHAR(20),
    lema VARCHAR(20),
    PRIMARY KEY (nombre));

CREATE TABLE PERSONA (
    nombre VARCHAR(20),
    edad VARCHAR(3),
    area VARCHAR(20),
    titulo VARCHAR(20),
    genelogia VARCHAR(30),
    nombreFAMILIA VARCHAR(20),
    nombreRELIGION VARCHAR(20),
    PRIMARY KEY (nombre),
    FOREIGN KEY (nombreFAMILIA) REFERENCES FAMILIA(nombre),
    FOREIGN KEY (nombreRELIGION) REFERENCES RELIGION(nombre));

CREATE TABLE ANTIGUO (
    nombre VARCHAR(20),
    nombreRELIGION VARCHAR(20),
    FOREIGN KEY (nombreRELIGION) REFERENCES RELIGION(nombre));

CREATE TABLE NUEVO (
    rhllor VARCHAR(20),
    el_gran_otro VARCHAR(20),
    nombreRELIGION VARCHAR(20),
    FOREIGN KEY (nombreRELIGION) REFERENCES RELIGION(nombre));
