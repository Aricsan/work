DROP DATABASE IF EXISTS starwars;
CREATE DATABASE starwars;
USE starwars;

CREATE TABLE ACTORES (
    Codigo TINYINT,
    Nombre VARCHAR(30),
    Fecha DATE,
    Nacionalidad VARCHAR(20),
    PRIMARY KEY (Codigo)
);

CREATE TABLE PERSONAJES (
    Codigo TINYINT,
    Nombre VARCHAR(30),
    Raza VARCHAR(20),
    Grado TINYINT,
    Codigo_Actores TINYINT,
    codigoSuperior_Personajes TINYINT,
    PRIMARY KEY (Codigo),
    FOREIGN KEY (Codigo_Actores) REFERENCES ACTORES(Codigo),
    FOREIGN KEY (codigoSuperior_Personajes) REFERENCES PERSONAJES(Codigo)
-- Esta foreign key es reflexiva y apunta a codigo, son los jefes.
);

CREATE TABLE PLANETAS (
    Codigo SMALLINT,
    Galaxia VARCHAR(20),
    Nombre VARCHAR(20),
    PRIMARY KEY (Codigo)
);

CREATE TABLE PELICULAS (
    Codigo TINYINT,
    Titulo VARCHAR(20),
    Director VARCHAR(20),
    Año DECIMAL(4,0),
    PRIMARY KEY (Codigo)
);

CREATE TABLE PERSONAJESPELICULAS (
    Codigo_Personajes TINYINT,
    Codigo_Peliculas TINYINT,
    FOREIGN KEY (Codigo_Personajes) REFERENCES PERSONAJES(Codigo),
    FOREIGN KEY (Codigo_Peliculas) REFERENCES PELICULAS(Codigo)
);

CREATE TABLE NAVES (
    Codigo SMALLINT,
    Nºtripulantes SMALLINT,
    Nombre VARCHAR(20),
    PRIMARY KEY (Codigo)
);

CREATE TABLE VISITAS (
    Codigo_Naves SMALLINT,
    Codigo_Planetas SMALLINT,
    Codigo_Peliculas TINYINT,
    FOREIGN KEY (Codigo_Naves) REFERENCES NAVES(Codigo),
    FOREIGN KEY (Codigo_Planetas) REFERENCES PLANETAS(Codigo),
    FOREIGN KEY (Codigo_Peliculas) REFERENCES PELICULAS(Codigo)
);


