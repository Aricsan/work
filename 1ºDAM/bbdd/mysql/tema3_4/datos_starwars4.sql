INSERT INTO ACTORES VALUES
    (159,'Charlie Cox','1992-03-24','Estadounidense'),
    (753,'Sienna Miller','1995-05-12','Britanica'),
    (684,'Michelle Pfeiffer','1989-08-18','Canadiense');

INSERT INTO PERSONAJES VALUES
    (6842,'Tristan Thorn','Hutt','General',159),
    (7952,'Victoria','Gorax','Capitan',753),
    (2864,'Yvaine','Jawa','Almirante',684);
