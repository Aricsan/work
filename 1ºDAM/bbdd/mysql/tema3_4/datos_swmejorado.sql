INSERT INTO ACTORES VALUES
    (NULL,'Carrie Fisher','1985-05-20','Polaca'),
    (NULL,'Mark Hamill','1975-09-15','Estadounidense'),
    (NULL,'Harrison Ford','1987-01-14','Estadounidense');

INSERT INTO PERSONAJES VALUES 
    (NULL,'Leia Organa','Ewok',5,1,'Jedi'),
    (NULL,'Luke Skywalker','Gorax',8,2,'Sith'),
    (NULL,'Han Solo','Jawa',4,3,'Sith');

INSERT INTO PLANETAS VALUES
    (NULL,'Homiden','Bespin'),
    (NULL,'Corellia','Ansion'),
    (NULL,'Naboo','Peragus');

INSERT INTO PELICULAS VALUES
    (NULL,'La amenaza fantasma','George Lucas','1999'),
    (NULL,'El ataque de los clones','George Lucas','2002'),
    (NULL,'La venganza de los Sith','George Lucas','2005');

INSERT INTO PERSONAJESPELICULAS VALUES
    (1,2),
    (3,1),
    (2,2);

INSERT INTO NAVES VALUES
    (NULL,10000,'Estrella de la Muerte'),
    (NULL,10,'Esclavo I'),
    (NULL,1,'Caza estelar Ala-X');
    
INSERT INTO VISITAS VALUES
    (1,2,3),
    (2,3,1),
    (3,1,3);
