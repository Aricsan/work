DROP DATABASE IF EXISTS ejercicio3f;
CREATE DATABASE ejercicio3f;
USE ejercicio3f;
CREATE TABLE MUEBLE (
    nombre VARCHAR(20),
    precio INT,
    PRIMARY KEY (nombre));

CREATE TABLE PIEZA (
    identificador INT,
    PRIMARY KEY (identificador));

CREATE TABLE ESTANTE (
    altura VARCHAR(2),
    pasillo VARCHAR(3),
    PRIMARY KEY (altura, pasillo));

CREATE TABLE TIENE (
    nombreMUEBLE VARCHAR(20),
    identificadorPIEZA INT,
    FOREIGN KEY (nombreMUEBLE) REFERENCES MUEBLE(nombre),
    FOREIGN KEY (identificadorPIEZA) REFERENCES PIEZA(identificador));

CREATE TABLE ESTA (
    identificadorPIEZA INT,
    alturaESTANTE VARCHAR(2),
    pasilloESTANTE VARCHAR(3),
    FOREIGN KEY (identificadorPIEZA) REFERENCES PIEZA(identificador),
    FOREIGN KEY (alturaESTANTE, pasilloESTANTE) REFERENCES ESTANTE(altura, pasillo));
