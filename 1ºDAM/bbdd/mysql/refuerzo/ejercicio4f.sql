DROP DATABASE IF EXISTS ejercicio4f;
CREATE DATABASE ejercicio4f;
USE ejercicio4f;
CREATE TABLE ESCENARIO (
    numero INT,
    riesgo VARCHAR(10),
    tiempo INT,
    PRIMARY KEY (numero));

CREATE TABLE PERSONAJE (
    nombre VARCHAR(20),
    habilidad VARCHAR(2),
    inteligencia VARCHAR(2),
    fuerza VARCHAR(2),
    hora INT,
    minutos INT,
    segundos INT,
    numeroESCENARIO INT,
    PRIMARY KEY (nombre),
    FOREIGN KEY (numeroESCENARIO) REFERENCES ESCENARIO(numero));

CREATE TABLE OBJETO (
    codigo INT,
    numeroESCENARIO INT,
    nombrePERSONAJE VARCHAR(20),
    PRIMARY KEY (codigo),
    FOREIGN KEY (numeroESCENARIO) REFERENCES ESCENARIO(numero),
    FOREIGN KEY (nombrePERSONAJE) REFERENCES PERSONAJE(nombre));
