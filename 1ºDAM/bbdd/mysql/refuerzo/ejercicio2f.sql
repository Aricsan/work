DROP DATABASE IF EXISTS ejercicio2f;
CREATE DATABASE ejercicio2f;
USE ejercicio2f;
CREATE TABLE TABLA (
    nombre VARCHAR(20),
    bbdd VARCHAR(20),
    PRIMARY KEY (nombre, bbdd));

CREATE TABLE CAMPO (
    nombre VARCHAR(20),
    tipo VARCHAR(20),
    nombreTABLA VARCHAR(20),
    bbddTABLA VARCHAR(20),
    PRIMARY KEY (nombre),
    FOREIGN KEY (nombreTABLA, bbddTABLA) REFERENCES TABLA(nombre, bbdd));

CREATE TABLE TIENE (
    nombreTABLA VARCHAR(20),
    bbddTABLA VARCHAR(20),
    nombreCAMPO VARCHAR(20),
    FOREIGN KEY (nombreTABLA, bbddTABLA) REFERENCES TABLA(nombre, bbdd),
    FOREIGN KEY (nombreCAMPO) REFERENCES CAMPO(nombre));
