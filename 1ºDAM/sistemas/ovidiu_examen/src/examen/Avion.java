package examen;

public class Avion extends Vehiculo {
	private byte motores;
	private double velocidadMax;
	private boolean combate;

	public Avion(byte motores, double velocidadMax, boolean combate, String marca, String color, String numero_bastidor, int kilmetros, int aniofabricacion) {
		super(marca, color, numero_bastidor, kilmetros, aniofabricacion);
		this.motores = motores;
		this.velocidadMax = velocidadMax;
		this.combate = combate;
	}
	
	public Avion(boolean combate, String marca, String color, String numero_bastidor, int kilmetros, int aniofabricacion) {
		super (marca, color, numero_bastidor, kilmetros, aniofabricacion);
		this.combate = combate;
		motores = -1;
		velocidadMax = -1.0;
	}

	public byte getMotores() {
		return motores;
	}

	public void setMotores(byte motores) {
		this.motores = motores;
	}

	public double getVelocidadMax() {
		return velocidadMax;
	}

	public void setVelocidadMax(double velocidadMax) {
		this.velocidadMax = velocidadMax;
	}

	public boolean isCombate() {
		return combate;
	}

	public void setCombate(boolean combate) {
		this.combate = combate;
	}

	@Override
	public String toString() {
		return "Avion [" + super.toString() + "motores=" + motores + ", velocidadMax=" + velocidadMax + ", combate=" + combate + "]";
	}

}
