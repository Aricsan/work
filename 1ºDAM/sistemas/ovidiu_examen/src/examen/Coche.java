package examen;

import java.util.Calendar;

public class Coche extends Vehiculo{
	private boolean electrico;
	private boolean antiguo;

	public Coche(boolean electrico, String marca, String color, String numero_bastidor, int kilmetros, int aniofabricacion) {
		super(marca, color, numero_bastidor, kilmetros, aniofabricacion);
		this.electrico = electrico;
		this.antiguo = check_antiguo (aniofabricacion);
	}

	private boolean check_antiguo(int anio_antiguo) {
		Calendar cal = Calendar.getInstance();
		int anio_actual = cal.get(Calendar.YEAR);
	
		if ((anio_actual - anio_antiguo) >= 25)
			return true;
		return false;
	}
	
	public boolean getElectrico() {
		return electrico;
	}

	public void setElectrico(boolean electrico) {
		this.electrico = electrico;
	}

	public boolean getAntiguo() {
		return antiguo;
	}

	public void setAntiguo(boolean antiguo) {
		this.antiguo = antiguo;
	}

	@Override
	public String toString() {
		return "Coche [" + super.toString() + "electrico=" + electrico + ", antiguo=" + antiguo + "]";
	}

}
