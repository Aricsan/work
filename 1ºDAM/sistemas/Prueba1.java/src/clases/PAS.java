package clases;

public class PAS extends Persona {
	private int Edad;
	private String Puesto;
	private String DNI;

	// Constructor
	public PAS(String nombre, String apellidos, int edad, String direccion, String puesto, String dNI) {
		super(nombre, apellidos, direccion);
		Edad = edad;
		Puesto = puesto;
		DNI = dNI;
	}

	// Función encargada de comprobar la validez del DNI
	private boolean dni_checker(String dni) {
		String numeros = "";																			// Variable que almacena los números del DNI
		String letra = "";																				// Variable que almacena la letra del DNI
		char DNI_mayus[] = dni.toUpperCase().toCharArray();												/* Convierto los caracteres del DNI a mayúscula 
																										   y a una variable de tipo char[] */
		
		char posib_letras[] = { 'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 			// Variable que almacena las posibles letras del DNI
								'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E' };
			
			/* Recorro el DNI comprobando si el valor en numérico, si lo es se 
			   almacena en números, si no se almacena en letra */
			for (int i = 0; i < DNI_mayus.length; i++) {
				if (Character.isDigit(DNI_mayus[i]))
					numeros += DNI_mayus[i];
				else
					letra += String.valueOf(DNI_mayus[i]);
			}

			// Convierto números a entero
			int numeros_int = Integer.parseInt(numeros);
			
			/* En el caso de que solo haya una letra y ésta además se corresponde, 
			   devuelvo true, en caso contrario devuelvo false */
			if (letra.length() == 1 && (posib_letras[numeros_int % 23] == letra.charAt(0)))
				return true;
			else
				return false;
		}

	public PAS() {
		super(null, null, null);
	}

	// Getters & Setters
	public int getEdad() {
		return Edad;
	}

	public void setEdad(int edad) {
		Edad = edad;
	}

	public String getPuesto() {
		return Puesto;
	}

	public void setPuesto(String puesto) {
		Puesto = puesto;
	}

	public String getDNI() {
		return DNI;
	}

	public void setDNI(String dNI) {
		if (dni_checker(dNI)) 
			DNI = dNI;
		
		else {
			System.out.println(dNI);
			throw new NumberFormatException();
		}
	}

	// toString
	@Override
	public String toString() {
		return "PAS [" + super.toString() + ", Edad=" + Edad + ", Puesto=" + Puesto + ", DNI" + DNI + "]";
	}

}
