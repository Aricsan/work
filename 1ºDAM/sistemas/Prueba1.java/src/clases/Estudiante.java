package clases;

public class Estudiante extends Persona {
	private int Anio_nacimiento;
	private String Curso;
	private String NIA;

	// Contructor
	public Estudiante(String nombre, String apellidos, String curso, int anio_nacimiento, String direccion, String nia) {
		super(nombre, apellidos, direccion);
		Curso = curso;
		Anio_nacimiento = anio_nacimiento;
		NIA = nia;
	}
	
	public Estudiante() {
		super(null, null, null);
	}

	// Getters & Setters
	public int getAnio_nacimiento() {
		return Anio_nacimiento;
	}

	public void setAnio_nacimiento(int anio_nacimiento) {
		Anio_nacimiento = anio_nacimiento;
	}

	public String getNIA() {
		return NIA;
	}

	public void setNIA(String nIA) {
		NIA = nIA;
	}

	public String getCurso() {
		return Curso;
	}

	public void setCurso(String curso) {
		Curso = curso;
	}

	// toString
	@Override
	public String toString() {
		return "Estudiante [" + super.toString() + ", Año Nacimiento=" + Anio_nacimiento + ", Curso=" + Curso + ", NIA=" + NIA + "]";
	}

}