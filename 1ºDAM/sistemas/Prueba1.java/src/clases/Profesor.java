package clases;

public class Profesor extends Persona {
	private String Asignatura;
	private String Cargo;
	private String Telefono;
	private String DNI;

	// Constructor
	public Profesor(String nombre, String apellidos, String direccion, String asignatura, String cargo, String telefono, String dni) {
		super(nombre, apellidos, direccion);
		Asignatura = asignatura;
		Cargo = cargo;
		Telefono = telefono;
		DNI = dni;
	}
	
	public Profesor() {
		super(null, null, null);
	}

	// Función encargada de comprobar la validez del DNI
	private boolean dni_checker(String dni) {
		String numeros = "";																			// Variable que almacena los números del DNI
		String letra = "";																				// Variable que almacena la letra del DNI
		char DNI_mayus[] = dni.toUpperCase().toCharArray();												/* Convierto los caracteres del DNI a mayúscula 
																										   y a una variable de tipo char[] */
		char posib_letras[] = { 'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 			// Variable que almacena las posibles letras del DNI
								'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E' };
		
		/* Recorro el DNI comprobando si el valor en numérico, si lo es se 
		   almacena en números, si no se almacena en letra */
		for (int i = 0; i < DNI_mayus.length; i++) {
			if (Character.isDigit(DNI_mayus[i]))
				numeros += DNI_mayus[i];
			else
				letra += String.valueOf(DNI_mayus[i]);
		}

		// Convierto números a entero
		int numeros_int = Integer.parseInt(numeros);
		
		/* En el caso de que solo haya una letra y ésta además se corresponde, 
		   devuelvo true, en caso contrario devuelvo false */
		if (letra.length() == 1 && (posib_letras[numeros_int % 23] == letra.charAt(0)))
			return true;
		else
			return false;
	}

	// Getters & Setters
	public String getAsignatura() {
		return Asignatura;
	}

	public void setAsignatura(String asignatura) {
		Asignatura = asignatura;
	}

	public String getCargo() {
		return Cargo;
	}

	public void setCargo(String cargo) {
		Cargo = cargo;
	}

	public String getTelefono() {
		return Telefono;
	}

	public void setTelefono(String telefono) {
		Telefono = telefono;
	}

	public String getDNI() {
		return DNI;
	}
	
	// Antes de asignar el valor del DNI lo comprobamos
	public void setDNI(String dNI) {
		if (dni_checker(dNI)) 
			DNI = dNI;
		
		else {
			System.out.println(dNI);
			throw new NumberFormatException();
		}
	}

	// toString
	@Override
	public String toString() {
		return "Profesor [" + super.toString() + ", Asignatura=" + Asignatura + ", Cargo=" + Cargo + ", Telefono=" + Telefono + ", DNI=" + DNI + "]";
	}

}
