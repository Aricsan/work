package clases;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Principal {

	static Scanner read = new Scanner(System.in);

	public static void main(String[] args) {

		int opcion;
		boolean salir = false;
		
		do {
			try {
				System.out.println("\t***Seleccione al usuario que quiere dar de Alta***");
				System.out.println("\t\t 1 --> Alumno\n\t\t 2 --> Profesor\n\t\t 3 --> Personal de Administración\n\t\t 4 --> Salir");
				opcion = read.nextInt();

				switch (opcion) {
				case 1:
					DatosAlumno(new Estudiante(), new Estudiante());																			/* Como parametro a las funciones le pasamos dos 
																																				/* Instancias de cada objeto de cada tipo de Clase*/
					break;
				case 2:
					DatosProfesor(new Profesor(), new Profesor());
					break;
				case 3:
					DatosPas(new PAS(), new PAS());
					break;
				case 4:
					System.out.println("\t***Gracias por usar nuestros servicios***");
					salir = true;
					break;
				default:
					System.out.println("Opcion Incorrecta");
				}

			} catch (InputMismatchException e) {																								// Tipo de error para Excepciones alfabéticas
				System.out.println("\t--ERROR--\nEl valor debe ser Numerico\n");
				read.next();																													// Quitamos el ultimo dato del buffer

			} catch (NumberFormatException e) {																									// Tipo de error para la Excepción DNI
				System.out.println("\t--ERROR--\nEl DNI introducido es Erroneo\n");

			} catch (IllegalArgumentException e) {																								// Tipo de error para las Excepciones numéricas
				System.out.println("\t--ERROR--\nEl valor debe ser Alfabetico\n");

			}

		} while (!salir);

		read.close();																															// Cerramos la connexión con el teclado
	}
	
	// Función encargada de asignar datos a los alumnos
	public static void DatosAlumno(Estudiante a, Estudiante b) {																				// Recibimos como parametros la instancia de dos objetos de tipo Estudiante

		System.out.println("Introduce los datos de los Alumnos separados por ENTER:\n");
		read.nextLine();																														// Descartamos todos los saltos de linea del buffer

		System.out.println("Nombres:");
		a.setNombre(string_checker(read.nextLine()));																							/* Utilizamos los Setters para asignar el valor que introduce el usuario 
																																				/* despues de comprobar que el String no tenga caracteres numericos*/
		b.setNombre(string_checker(read.nextLine()));
		System.out.println("Apellidos:");
		a.setApellidos(string_checker(read.nextLine()));
		b.setApellidos(string_checker(read.nextLine()));

		System.out.println("Cursos:");
		a.setCurso(read.nextLine());
		b.setCurso(read.nextLine());

		System.out.println("Direcciones:");
		a.setDireccion(read.nextLine());
		b.setDireccion(read.nextLine());

		System.out.println("NIAs:");
		a.setNIA(read.nextLine());
		b.setNIA(read.nextLine());

		System.out.println("Años de Nacimiento:");
		a.setAnio_nacimiento(read.nextInt());
		b.setAnio_nacimiento(read.nextInt());
		
		// Mostramos los datos de los objetos
		System.out.println("\nLos Alumnos " + a.getNombre() + " y " + b.getNombre() + " han sido dados de alta con los siguientes datos:\n\n" + a + "\n" + b + "\n");
	}
	
	// Función encargada de asignar datos a los profesores
	public static void DatosProfesor(Profesor a, Profesor b) {																					// Recibe como parametros la instancia de dos objetos del tpo Profesor
		System.out.println("Introduce los datos del Profesor separados por ENTER:\n");
		read.nextLine();

		System.out.println("Nombres:");
		a.setNombre(string_checker(read.nextLine()));
		b.setNombre(string_checker(read.nextLine()));

		System.out.println("Apellidos: ");
		a.setApellidos(string_checker(read.nextLine()));
		b.setApellidos(string_checker(read.nextLine()));

		System.out.println("Direcciones:");
		a.setDireccion(read.nextLine());
		b.setDireccion(read.nextLine());

		System.out.println("Asignaturas dadas: ");
		a.setAsignatura(string_checker(read.nextLine()));
		b.setAsignatura(string_checker(read.nextLine()));

		System.out.println("DNIs:");
		a.setDNI(read.nextLine());
		b.setDNI(read.nextLine());

		System.out.println("Cargos:");
		a.setCargo(string_checker(read.nextLine()));
		b.setCargo(string_checker(read.nextLine()));

		System.out.println("Telefonos:");
		a.setTelefono(read.nextLine());
		b.setTelefono(read.nextLine());

		System.out.println("\nLos profesor " + a.getNombre() + " y " + b.getNombre() + " han sido dados de alta con los siguientes datos:\n\n" + a + "\n" + b + "\n");
	}
	
	// Función encargada de asignar los datos del Personal de Administración
	public static void DatosPas(PAS a, PAS b) {																									// Recibe como parametros la instancia de dos objetos del tipo PAS
		System.out.println("Introduce los datos del Personal de Administración separados por ENTER:\n");
		read.nextLine();

		System.out.println("Nombres:");
		a.setNombre(string_checker(read.nextLine()));
		b.setNombre(string_checker(read.nextLine()));

		System.out.println("Apellidos:");
		a.setApellidos(string_checker(read.nextLine()));
		b.setApellidos(string_checker(read.nextLine()));

		System.out.println("Direcciones:");
		a.setDireccion(read.nextLine());
		b.setDireccion(read.nextLine());

		System.out.println("Puestos:");
		a.setPuesto(string_checker(read.nextLine()));
		b.setPuesto(string_checker(read.nextLine()));
		
		System.out.println("DNIs:");
		a.setDNI(read.nextLine());
		b.setDNI(read.nextLine());

		System.out.println("Edades:");
		a.setEdad(read.nextInt());
		b.setEdad(read.nextInt());

		System.out.println("\nEl Personal de Administración " + a.getNombre() + " y " + b.getNombre() + " han sido dados de alta con los siguientes datos:\n\n" + a + "\n" + b + "\n");
	}
	
	// Función encargada de comprobar si una cadena contienen caracteres numéricos
	public static String string_checker(String cadena) {
		char char_cadena[] = cadena.toLowerCase().toCharArray();

		for (int i = 0; i < char_cadena.length; i++)
			if (Character.isDigit(char_cadena[i]))
				throw new IllegalArgumentException();																							// Si la cadena dada contiene numeros, producimos una Excepción que recogeremos en main

		return cadena;
	}
}
