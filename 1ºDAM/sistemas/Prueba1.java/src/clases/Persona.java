package clases;

public class Persona {

	private String Nombre;
	private String Apellidos;
	private String Direccion;

	// Constructor Padre
	public Persona(String nombre, String apellidos, String direccion) {
		Nombre = nombre;
		Apellidos = apellidos;
		Direccion = direccion;
	}

	// Getters & Setters in commun
	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public String getApellidos() {
		return Apellidos;
	}

	public void setApellidos(String apellidos) {
		Apellidos = apellidos;
	}

	public String getDireccion() {
		return Direccion;
	}

	public void setDireccion(String direccion) {
		Direccion = direccion;
	}

	// toString in commun
	@Override
	public String toString() {
		return "Nombre=" + Nombre + ", Apellidos=" + Apellidos + ", Direccion=" + Direccion;
	}
}
