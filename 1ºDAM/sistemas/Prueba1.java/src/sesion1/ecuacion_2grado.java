package sesion1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ecuacion_2grado {

	/*
	 * Ecuación de segundo grado -> ax² + bx + c = 0 Formula de solución
	 * 
	 * -b±√b²-4ac x = --------------- 2a
	 */

	public static void main(String[] args) {

		double a = 0, b = 0, c = 0, discriminante;
		int ok = 0;

		Scanner datos = new Scanner(System.in);

		// Pedir Datos al Usuario
		while (ok == 0) {
			try {
				System.out.println("Bienvenido, introduce una ecuación de segundo grado para su posterior solución:");
				System.out.println("\t ax² + bx + c = 0");

				System.out.println("\nIntroduzca un valor para a");
				a = datos.nextDouble();// Asignación del valor
				System.out.println("Introduza un valor para b");
				b = datos.nextDouble();
				System.out.println("Introduzca un valor para c");
				c = datos.nextDouble();

				System.out.println("¿Es correcta la siguiente ecuación? \n\n\t0 --> no \n\t1 --> si\n");
				System.out.println(a + "x² + " + b + "x + " + c + " = 0");
				ok = datos.nextInt();
			} catch (InputMismatchException e) {
				System.out.println("Dato Erroneo");
				datos.next();
			}
		}

		// Calculos
		discriminante = Math.pow(b, 2) - 4 * a * c;

		// Si el discriminante es mayor o igual a 0 la ecuacion tiene solución
		if (discriminante >= 0) {
			System.out.println("\nSolución 1 --> " + (-b + Math.sqrt(discriminante)) / (2 * a));
			System.out.println("Solución 2 --> " + (-b - Math.sqrt(discriminante)) / (2 * a));

		}
		// Si por el contrario es menor que 0 no tiene solución
		else
			System.out.println("La ecuacion no tiene una solución Real");

		datos.close();
	}
}
