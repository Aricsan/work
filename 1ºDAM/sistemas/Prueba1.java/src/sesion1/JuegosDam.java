package sesion1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class JuegosDam {

	// Variables static utilizadas en toda la calse
	static Scanner leer = new Scanner(System.in);
	static int opcion_minijuego;

	public static void main(String[] args) {
		int opcion_juego;
		boolean salir = false;

		System.out.println("\t-----Bienvenido a DAM Plays-----");
		do {
			try {
				System.out.println("Para Jugar debe seleccionar una de estas opciones");
				System.out.println("\t 1 --> Juegos Figuras\n\t 2 --> Juegos de Palabras\n\t 3 --> Salir del Juego");
				opcion_juego = leer.nextInt();

				// Mediante diferentes funciones el programa realiza lo pedido por el usuario
				switch (opcion_juego) {
				case 1:
					figuras();
					break;
				case 2:
					palabras();
					break;
				case 3:
					System.out.println("\t--- Gracias por jugar ---\n\t --- Vuelve Pronto ---");
					salir = true;
					break;
				default:
					System.out.println("Opcion Incorrecta");
				}
			} catch (InputMismatchException e) { // Excepcion que se produce al insertar un token que no coincide con la
													// expresión regular entera o está fuera de rango
				System.out.println("Dato Erroneo");
				leer.next(); // Limpiar el Buffer
			}
		} while (!salir);

		leer.close();
	}

	public static void figuras() {
		int lado;

		System.out.print("Indique el lado de la figura: ");
		lado = leer.nextInt();
		System.out.println(
				"¿Que figura desea mostrar?\n\t 1 --> Cuadrado\n\t 2 --> Triangulo Rectangulo\n\t 3 --> Rombo");
		opcion_minijuego = leer.nextInt();

		// Las figuras estas dividadas en funciones para una mayor comprensión del
		// codigo
		switch (opcion_minijuego) {
		case 1:
			cuadrado(lado);
			break;
		case 2:
			triangulo(lado);
			break;
		case 3:
			rombo(lado);
			break;
		default:
			System.out.println("Opcion Incorrecta");
		}
	}

	public static void cuadrado(int lado) {

		for (int f = 0; f < lado; f++) { // Numero de filas
			for (int c = 0; c < lado; c++) // Numero de columnas
				if (f == 0 || f == lado - 1 || c == 0 || c == lado - 1)
					System.out.print("#");
				else
					System.out.print(" ");
			System.out.print("\n");
		}
	}

	public static void triangulo(int lado) {

		for (int f = 0; f < lado; f++) { // Numero de filas
			for (int c = 0; c <= f; c++) // Numero de columnas
				if (c == 0 || c == f || f == lado - 1)
					System.out.print("# ");
				else
					System.out.print("  ");
			System.out.print("\n");
		}
	}

	public static void rombo(int lado) {

		// Pintas piramide superior
		for (int f = 0; f <= lado; f++) { // Numero de filas
			for (int c = 0; c < lado - f; c++) // Numero de columnas
				System.out.print(" ");
			for (int r = 0; r < f * 2 - 1; r++) // Bucle que se encarga de rellenar la figura
				System.out.print("#");
			System.out.println();
		}
		// Pintar piramide inferior
		for (int f = lado; f >= 0; f--) {
			for (int c = lado - f; c > 0; c--)
				System.out.print(" ");
			for (int r = f * 2 - 1; r > 0; r--)
				System.out.print("#");
			System.out.print("\n");
		}
	}

	public static void palabras() {
		String palabra, vocales = "", consonantes = ""; // Es necesario inicializar vocales y consonantes debido a que
														// almaceno en ellas las letras correspondientes
		char letra[];

		System.out.println("Inserte una Palabras o Palabras");
		leer.nextLine(); // Al leer un dato por teclado el \n o salto de linea queda en el buffer, antes
							// de leer cualquier caracter es necesario quitarselo
		// leer.nextLine();
		palabra = leer.nextLine(); // Metodo para leer por teclado un String

		try {
			if (Integer.parseInt(palabra) > 0 || Integer.parseInt(palabra) < 0) {
				System.out.println("Dato Erroneo");
				palabras();
			}
		} catch (NumberFormatException e) {};

		letra = palabra.toLowerCase().toCharArray(); /*
														 * Mediante toCharArray() convierto la cadena palabra en un
														 * array de caracteres Mediante toLowerCase() convierto la
														 * cadena a todo minisculas para poder realizar la comprobación
														 */
		for (int i = 0; i < palabra.length(); i++)
			if (letra[i] == 'a' || letra[i] == 'e' || letra[i] == 'i' || letra[i] == 'o' || letra[i] == 'u')
				vocales += letra[i];
			else
				consonantes += letra[i];

		// Funcion encarga de ordenar alfabéticamente las letras recibidas
		ordenar(vocales, "Vocales");
		ordenar(consonantes, "Consonantes");

	}

	public static void ordenar(String letras, String tipo_letra) {
		char letras_ordenadas[] = letras.toCharArray(); // Convierto la cadena letras en un array de caracteres
		char aux; // La variable aux almacena un caracter temporal para su posicionamiento en el
					// array

		for (int i = 0; i < letras.length() - 1; i++) // Bucle encargado de recorrer la longitud de la cadena. La ultima
														// letra no hace falta ordenarla
			for (int j = 0; j < letras.length() - 1; j++) // Bucle encargado de comparar las dos letras
				if (letras_ordenadas[j] > letras_ordenadas[j + 1]) {
					aux = letras_ordenadas[j + 1];
					letras_ordenadas[j + 1] = letras_ordenadas[j];
					letras_ordenadas[j] = aux;
				}
		// Es necesario crear un String al imprimir debido a la sobrecarga de metodo, ya
		// que al añadir texto interpreta que lo que va a imprimir es de tipo String
		System.out.println(tipo_letra + " Ordenadas: " + new String(letras_ordenadas));
	}
}
