package sesion1;

public class Prueba2 {

	public static void main(String[] args) {
		String fragmentoNombreAlumnos = "Victor";
		String[] nombreAlumnos = {"Antonio", "Marta", "Victor Hugo", "David"};
		
		int resultado = contarUsuarios(fragmentoNombreAlumnos, nombreAlumnos);
		System.out.println("Total resultados: " + resultado);
		
		

	}
	
	static int contarUsuarios(String fragmentoNombreAlumno, String[] nombreAlumnos) {
		boolean encontrado = false;
		int totalEncontrados = 0;
		
		for (String nombreAlumnoActual : nombreAlumnos) {
			if (nombreAlumnoActual.contains(fragmentoNombreAlumno)) {
				encontrado = true;
			}
			if (encontrado) {
				totalEncontrados++;
				encontrado = false;
				/* El problema estaba en que al enontrar Victor en nombreAlumnos, encontrado
				 * pasa a valer true, y totalEncontrados se incremnta en 1, pero com ono ser vuelve 
				 * a poner encontrado a false se incrementa indistintamente si entra o no en 
				 * el primer if. La sulucion es, despues de incremnetar totalEncontrados igualar
				 * encontrado a false, de esta manera solo se incrementara si encuentra la cadena especificada*/
				
			}
		}
		return totalEncontrados;
	}
}
