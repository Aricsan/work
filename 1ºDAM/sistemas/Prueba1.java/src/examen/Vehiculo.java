package examen;

public class Vehiculo {
	private String marca;
	private String color;
	private String numero_bastidor;
	private int kilmetros;
	private int aniofabricacion;
	
	
	protected Vehiculo(String marca, String color, String numero_bastidor, int kilmetros, int aniofabricacion) {
		this.marca = marca;
		this.color = color;
		this.numero_bastidor = numero_bastidor;
		this.kilmetros = kilmetros;
		this.aniofabricacion = aniofabricacion;
	}

	protected String getMarca() {
		return marca;
	}


	protected void setMarca(String marca) {
		this.marca = marca;
	}


	protected String getColor() {
		return color;
	}


	protected void setColor(String color) {
		this.color = color;
	}


	protected String getNumero_bastidor() {
		return numero_bastidor;
	}


	protected void setNumero_bastidor(String numero_bastidor) {
		this.numero_bastidor = numero_bastidor;
	}


	protected int getKilmetros() {
		return kilmetros;
	}


	protected void setKilmetros(int kilmetros) {
		this.kilmetros = kilmetros;
	}


	protected int getAniofabricacion() {
		return aniofabricacion;
	}


	protected void setAniofabricacion(int aniofabricacion) {
		this.aniofabricacion = aniofabricacion;
	}

	@Override
	public String toString() {
		return "Vehiculo [marca=" + marca + ", color=" + color + ", numero_bastidor=" + numero_bastidor + ", kilmetros="
				+ kilmetros + ", aniofabricacion=" + aniofabricacion + "]";
	}
	
	
	

}
