package examen;

public class Barco extends Vehiculo{
	private int eslora;
	private int calado;
	private enum Tipo {Vela, Pesca, Pasajeros}
	private Tipo tipo; 

	public Barco( int eslora, int calado, Tipo tipo, String marca, String color, String numero_bastidor, int kilmetros, int aniofabricacion) {
		super(marca, color, numero_bastidor, kilmetros, aniofabricacion);
		this.eslora = eslora;
		this.calado = calado;
		this.tipo = tipo;
	}

	public int getEslora() {
		return eslora;
	}

	public void setEslora(int eslora) {
		this.eslora = eslora;
	}

	public int getCalado() {
		return calado;
	}

	public void setCalado(int calado) {
		this.calado = calado;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "Barco [" + super.toString() + "eslora=" + eslora + ", calado=" + calado + ", tipo=" + tipo + "]";
	}

}
