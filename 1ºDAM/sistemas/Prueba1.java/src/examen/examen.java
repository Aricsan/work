package examen;

import java.util.InputMismatchException;
import java.util.Scanner;

public class examen {
	
	static Scanner read = new Scanner(System.in);
	
	public static void main(String[] args) {
		int opcion;
		boolean salir = false;
		
		do {
			try {
				System.out.println("Elige una opcion");
				System.out.println("1 --> Coche\n 2 --> Barco\n 3 --> Avion");
				opcion = read.nextInt();

				switch (opcion) {
				case 1:
					Coche ca = crear_var_coche();
					
					break;
				case 2:
					//Barco ba = crear_var_barco();
					break;
				case 3:

					break;
				case 4:
					
					salir = true;
					break;
				default:
					System.out.println("Opcion Incorrecta");
				}

			} catch (InputMismatchException e) {
				System.out.println("\t--ERROR--\nEl valor debe ser Numerico\n");
				read.next();
			}

		} while (!salir);

		read.close();

	}
	
	 private static Coche crear_var_coche() {
		boolean electrico;
		String marca;
		String color;
		String num_bastidor;
		int kilome;
		int anio_fabri;
		
		read.next();
		
		System.out.print("Inserte si el coche es Eléctrico: ");
		electrico = read.nextBoolean();
		
		System.out.print("Inserte la marca del coche: ");
		marca = read.nextLine();
		
		System.out.print("Inserte el color del coche: ");
		color = read.nextLine();
		
		System.out.print("Inserte el numero de bastidor del coche: ");
		num_bastidor = read.nextLine();
		
		System.out.print("Inserte el numero de Kilometros del coche: ");
		kilome = read.nextInt();
		
		System.out.print("Inserte el Año de Fabricacion del coche: ");
		anio_fabri = read.nextInt();
		
		Coche tmp = new Coche(electrico, marca, color, num_bastidor, kilome, anio_fabri);
		return tmp;	
	}
	
	/*private static Barco crear_var_barco() {
		int eslora;
		int calado;
		String tipo;	
		String marca;
		String color;
		String num_bastidor;
		int kilome;
		int anio_fabri;
		
		read.next();
		
		System.out.print("Inserte la eslora del Barco: ");
		eslora = read.nextInt();
		
		System.out.print("Inserte el calado del Barco: ");
		calado = read.nextInt();
		
		System.out.print("Inserte el tipo : ");
		tipo = read.nextLine();
		
		System.out.print("Inserte el numero de bastidor del coche: ");
		num_bastidor = read.nextLine();
		
		System.out.print("Inserte el numero de Kilometros del coche: ");
		kilome = read.nextInt();
		
		System.out.print("Inserte el Año de Fabricacion del barco: ");
		anio_fabri = read.nextInt();
		
		Barco tmp = new Barco(eslora, calado, tipo, color, num_bastidor, kilome, anio_fabri);
		return tmp;	
	}*/
}
