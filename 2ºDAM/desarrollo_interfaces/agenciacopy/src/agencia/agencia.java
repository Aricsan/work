package agencia;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class agencia {

	static Scanner read = new Scanner(System.in);
	static Itinerario itinerario;
	static ArrayList<String> array = new ArrayList<String>();
	static int a = 0;

	public static void main(String[] args)
	{
		// Variables
		FicheroItinerarios fichero = new FicheroItinerarios();

		int opcion; // Opción elegida por el usuario
		String nombre;
		boolean salir = false; // Variable de tipo bool para terminar

		// try catch
		System.out.print("Introduzca el nombre del fichero: ");
		nombre = read.next();

		do {
			try {
				menu();
				opcion = read.nextInt();
				switch (opcion) {
				case 1:
					fichero.abrir(nombre, true);
					fichero.leer(itinerario, true);
					fichero.cerrar();
					break;
				case 2:// echo
					crear_itinerario();
				case 3:

					break;
				case 4: // echo
					itinerario.borrar();
					break;
				case 5:
					fichero.abrir(nombre, false);

					fichero.escribir(crear_itinerario());
					break;
				case 6: // echo
					fichero.abrir(nombre, false);
					fichero.escribir(itinerario);
					fichero.cerrar();
					break;
				case 7:

					break;
				case 8: // echo
					System.out.println(itinerario.toString());
					break;
				case 9: // echo
					ordenar(nombre, true);
					break;
				case 10: // echo
					ordenar(nombre, false);
					break;
				case 11:

					break;
				case 12:
					System.out
							.println("\t--- Gracias por utilizar nustros servicios ---\n\t\t    --- Hasta Pronto ---");
					salir = true;
					break;
				default:
					System.out.println("Opcion Incorrecta");
				}
			} catch (InputMismatchException e) {
				System.out.println("Dato Erroneo");
				read.next(); // Limpiar Buffer de espacios tabulaciones y saltos de linea
			} catch (IOException e) {
				// ficehro al crear
				e.printStackTrace();
				// e.getMessage();
			}
		} while (!salir);

		read.close(); // Cerrar objeto Scanner

	}

	// Metodo menu
	public static void menu()
	{
		System.out.println(
				" Elija una opción\n\t1. Leer fichero\n\t2. Insertar\n\t3. Borrar\n\t4. Vaciar\n\t5. Modificar\n\t6. Guardar fichero\n\t7. Separar fichero\n\t"
						+ "8. Mostrar contenido\n\t9. Mostrar Itinerarios de menor a mayor\n\t10. Mostrar Itinerarios de mayor a menor\n\t11. Mostrar destino mas repedido"
						+ "\n\t12. Salir");
	}

	public static Itinerario crear_itinerario()
	{
		String nombre;
		int cant_dest;

		System.out.print("Introduzca el nombre del Itinerario: ");
		array.add(nombre = read.next());
		System.out.print("Introduzca la cantidad de destinos: ");
		array.add(Integer.toString(cant_dest = read.nextInt()));

		for (int i = 0; i < cant_dest; i++) {
			System.out.print("Introduzca el destino nº " + (i + 1) + ": ");
			array.add(read.next());
		}
		array.add("-1");
		a += (cant_dest + 3);

		itinerario = new Itinerario(nombre, array, a);
		return itinerario;
	}

	public static void ordenar(String nombre_fichero, boolean menor) throws IOException
	{
		File file = new File(nombre_fichero);
		FileReader fileR = new FileReader(file);
		BufferedReader fileBr = new BufferedReader(fileR);
		ArrayList<Integer> cant_dest = new ArrayList<Integer>();
		ArrayList<String> nombre_iti = new ArrayList<String>();
		String final_iti;
		String lin_sig;
		String aux_cadena;
		int aux_num;

		nombre_iti.add(fileBr.readLine());
		cant_dest.add(Integer.parseInt(fileBr.readLine()));

		while ((final_iti = fileBr.readLine()) != null)
			if (final_iti.equals("-1") && (lin_sig = fileBr.readLine()) != null) {
				nombre_iti.add(lin_sig);
				cant_dest.add(Integer.parseInt(fileBr.readLine()));
			}
		
		if (menor) {
			// menor a mayor
			for (int i = 0; i < cant_dest.size() - 1; i++)
				for (int j = 0; j < cant_dest.size() - 1; j++)
					if (cant_dest.get(j) > cant_dest.get(j + 1)) {
						aux_num = cant_dest.get(j + 1);
						aux_cadena = nombre_iti.get(j + 1);
						cant_dest.set(j + 1, cant_dest.get(j));
						nombre_iti.set(j + 1, nombre_iti.get(j));
						cant_dest.set(j, aux_num);
						nombre_iti.set(j, aux_cadena);
					}
		} else {
			// mayor a menor
			for (int i = 0; i < cant_dest.size() - 1; i++)
				for (int j = 0; j < cant_dest.size() - 1; j++)
					if (cant_dest.get(j) < cant_dest.get(j + 1)) {
						aux_num = cant_dest.get(j + 1);
						aux_cadena = nombre_iti.get(j + 1);
						cant_dest.set(j + 1, cant_dest.get(j));
						nombre_iti.set(j + 1, nombre_iti.get(j));
						cant_dest.set(j, aux_num);
						nombre_iti.set(j, aux_cadena);
					}
		}
		System.out.println(cant_dest);
		System.out.println(nombre_iti);
		fileBr.close();
	}
}
