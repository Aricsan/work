package agencia;

import java.io.*;

public class FicheroItinerarios {

	private File file;
	private FileWriter fileW;
	private FileReader fileR;
	private BufferedReader fileBr;
	private BufferedWriter fileBw;
	private boolean lectura;

	public FicheroItinerarios()
	{
	}

	public void abrir(String nombre, boolean lectura) throws IOException
	{
		file = new File(nombre);
		this.lectura = lectura;

		if (file.createNewFile())
			System.out.println("Fichero " + nombre + " ha sido creado");
		else
			System.out.println("Fichero " + nombre + " no ha sido creado");

		if (lectura)
			fileR = new FileReader(file);
		else
			fileW = new FileWriter(file);
	}

	public String leer(Itinerario itinerario, boolean leer_fichero) throws IOException
	{
		fileBr = new BufferedReader(fileR);
		lectura = true;
		String buscar;
		String destino;
		String linea = "";

		if (!leer_fichero) {
			while (!(buscar = fileBr.readLine()).equals(itinerario.getNombre()))
				;

			linea += buscar + "\n";
			while (!(destino = fileBr.readLine()).equals("-1"))
				linea += destino + "\n";
		} else
			leer_fichero(linea, fileBr);

		return linea;
	}

	private void leer_fichero(String linea, BufferedReader fileBr) throws IOException
	{
		String final_fichero = "";

		while ((final_fichero = fileBr.readLine()) != null)
			linea += final_fichero + "\n";
	}

	public void escribir(Itinerario itinerario) throws IOException
	{
		fileBw = new BufferedWriter(fileW);

		for (int i = 0; i < itinerario.getCant_destinos(); i++) {
			fileBw.write(itinerario.getDestinos().get(i));
			fileBw.newLine();
		}

		//fileBw.write("-1");
	}

	public void cerrar() throws IOException
	{
		if (lectura)
			fileBr.close();
		else
			fileBw.close();
	}

	/*
	 * public boolean Final() {
	 * 
	 * return; }
	 */

	public void vaciar() throws IOException
	{
		fileR = new FileReader(file);
		fileW = new FileWriter(file);
		fileBw = new BufferedWriter(fileW);
		fileBr = new BufferedReader(fileR);

		while (fileBr.readLine() != null)
			fileBw.write(" ");
	}
}
