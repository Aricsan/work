package agencia;

import java.util.ArrayList;

public class Itinerario {

	private String nombre;
	private int cant_destinos;
	private ArrayList<String> array;

	public Itinerario(String nombre, ArrayList<String> destinos, int cant_destinos)
	{
		this.nombre = nombre;
		this.array = destinos;
		this.cant_destinos = cant_destinos;
	}
	
	public void borrar() {
		array.clear();
	}
	
	public String getNombre()
	{
		return nombre;
	}

	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}

	public int getCant_destinos()
	{
		return cant_destinos;
	}

	public void setCant_destinos(int cant_destinos)
	{
		this.cant_destinos = cant_destinos;
	}

	public ArrayList<String> getDestinos()
	{
		return array;
	}

	public void setDestinos(ArrayList<String> destinos)
	{
		this.array = destinos;
	}

	@Override
	public String toString()
	{
		return array + "\n";
	}


}
