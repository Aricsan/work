package titular;

import java.awt.EventQueue;

import javax.swing.JFrame;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JLabel;

public class Titular {

	private JFrame frame;
	private JLabel lblNewLabelNoticia;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Titular window = new Titular();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Titular() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 450, 300);
		frame.setLocationRelativeTo(null); // situa la ventana en el centro de la pantalla
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null); // lo mismo que absolute
		
		JButton ButtonNoticia = new JButton("Buscar Noticia El Mundo");
		ButtonNoticia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					titular();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		ButtonNoticia.setBounds(109, 11, 204, 51);
		frame.getContentPane().add(ButtonNoticia);
		
		lblNewLabelNoticia = new JLabel("pepe");
		lblNewLabelNoticia.setBounds(10, 116, 414, 134);
		lblNewLabelNoticia.setHorizontalAlignment(SwingConstants.CENTER); // centrar el texto en jlabel (es un textfield pero desabilitado)
		frame.getContentPane().add(lblNewLabelNoticia);
	}
	
	private void titular() throws IOException {
		Document document;
		String webPage = "https://www.elmundo.es/economia/macroeconomia/2020/10/27/5f97deb2fc6c832a478b458d.html";
		document = Jsoup.connect(webPage).get();
		Elements palabra = document.getElementById("H1_js_5f97deb2fc6c832a478b458d").getElementsByClass("ue-c-article__headline js-headline");	
		lblNewLabelNoticia.setText(palabra.get(0).html());
	}
}
