package arbol_directorios;

import java.io.File;
import java.io.IOException;

public class Principal {

	public static void main(String[] args) {

		String tree[][] = { 
				{ "estilos", "css4", "estilo1.css", "estilo2.css", "css3", "estilo3.css" },
				{ "script", "js", "script1.js", "script2.js", "php", "cabecera.php", "modelo.php" },
				{ "imagenes", "alta", "paisaje.jpg", "media", "paisaje.jpg", "baja", "paisaje.jpg" }};

		File proyecto = new File("Proyecto");
		proyecto.mkdir();
		File file;
		File ultimo_dir = null;

		try {

			for (int i = 0; i < tree.length; i++) {
				file = new File(proyecto, tree[i][0]);
				file.mkdir();
				for (int j = 1; j < tree[i].length; j++) {
					if (!tree[i][j].contains(".")) {
						file = new File(tree[i][0], tree[i][j]);
						file.mkdir();
						ultimo_dir = file;
					} else {
						file = new File(ultimo_dir, tree[i][j]);
						file.createNewFile();
					}
				}
			}

		} catch (IOException e) {
			System.out.println("Error en la creación: " + e.getMessage());
		}
	}
}