package paneles_una_ventana;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Ficheros {

	private File usuarios;
	private FileReader fileR;
	private FileWriter fileW;
	private BufferedReader fileBr;
	private BufferedWriter fileBw;

	public Ficheros(String nombre)
	{
		usuarios = new File(nombre);
	}

	public String leer() throws IOException
	{
		fileR = new FileReader(usuarios);
		fileBr = new BufferedReader(fileR);

		String linea = "";
		String buscar;

		while ((buscar = fileBr.readLine()) != null) {
			linea += buscar + "\n";
		}
		
		fileBr.close();
		return linea;
	}
	
	public void escribir (String user, String passw) throws IOException 
	{
		fileW = new FileWriter(usuarios.getAbsoluteFile(), true); // el true si que concatena
		fileBw = new BufferedWriter(fileW);
		
		fileBw.write(user + ":" + passw);
		fileBw.newLine();
		fileBw.newLine();

		fileBw.close();		
	}
}