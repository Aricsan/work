package paneles_una_ventana;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class GribPanel {

	private JFrame frame;

	JPanel panel_1 = new JPanel();
	JPanel panel_2 = new JPanel();
	JPanel panel_3 = new JPanel();
	File file = new File("usuarios");
	JLabel lblNewLabel_1 = new JLabel("texto");
	private JTextField user;
	private JTextField pass;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable() {
			public void run()
			{
				try {
					GribPanel window = new GribPanel();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GribPanel()
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.RED);
		// frame.setBounds(100, 100, 450, 300);
		frame.setResizable(false);	
		frame.setSize(600, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.getContentPane().setLayout(new GridLayout(2, 2, 5, 5));

		JPanel panel = new JPanel();
		panel.setBackground(Color.GREEN);
		frame.getContentPane().add(panel);
		panel.setLayout(null);

		JButton btnNewButton_1 = new JButton("Iniciar Sesion");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0)
			{
				try {
					if (leer(file))
						panel_1.setVisible(true);
					else
						System.out.println("No se ha encontrado el usuario");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnNewButton_1.setBounds(157, 138, 102, 23);
		panel.add(btnNewButton_1);

		JLabel lblNewLabel_2 = new JLabel("Usuario: ");
		lblNewLabel_2.setBounds(37, 45, 69, 14);
		panel.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("Contrase\u00F1a:");
		lblNewLabel_3.setBounds(37, 93, 69, 14);
		panel.add(lblNewLabel_3);

		user = new JTextField();
		user.setBounds(152, 42, 86, 20);
		panel.add(user);
		user.setColumns(10);

		pass = new JPasswordField();
		pass.setBounds(152, 90, 86, 20);
		panel.add(pass);
		pass.setColumns(10);

		JLabel lblNewLabel_4 = new JLabel("Iniciar Sesi\u00F3n o Registrarse");
		lblNewLabel_4.setBounds(73, 11, 137, 14);
		panel.add(lblNewLabel_4);
		
		JButton btnNewButton_3 = new JButton("Registrarse");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					if (leer(file));
						// ventanada ese nombre de usuario ya existe
					else
						escribir(user.getText() + ":" + pass.getText(), file);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnNewButton_3.setBounds(17, 138, 102, 23);
		panel.add(btnNewButton_3);

		panel_1.setBackground(Color.CYAN);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		panel_1.setVisible(false);

		JButton btnNewButton = new JButton("Activar panel 2");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0)
			{
				panel_2.setVisible(true);

			}
		});
		btnNewButton.setBounds(55, 94, 118, 23);
		panel_1.add(btnNewButton);

		JTextArea textArea = new JTextArea();
		textArea.setBounds(55, 24, 108, 41);
		panel_1.add(textArea);

		panel_2.setBackground(Color.BLUE);
		frame.getContentPane().add(panel_2);
		panel_2.setLayout(null);
		panel_2.setVisible(false);

		JButton btnNewButton_2 = new JButton("Activar panel 3");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0)
			{
				panel_3.setVisible(true);
				/*
				 * try { escribir(textArea.getText()); lblNewLabel_1.setText(leer()); } catch
				 * (IOException e) { // TODO Auto-generated catch block e.printStackTrace(); }
				 */
			}
		});
		btnNewButton_2.setBounds(51, 94, 121, 23);
		panel_2.add(btnNewButton_2);

		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\DAM.LAB35-PC13\\Downloads\\\u00EDndice.jpg"));
		lblNewLabel.setBounds(0, 0, 214, 128);
		panel_2.add(lblNewLabel);

		panel_3.setBackground(Color.ORANGE);
		frame.getContentPane().add(panel_3);
		panel_3.setLayout(null);

		lblNewLabel_1.setBounds(77, 54, 75, 41);
		panel_3.add(lblNewLabel_1);
		panel_3.setVisible(false);
	}

	private void escribir(String texto, File file) throws IOException
	{
		Scanner read = new Scanner(System.in);
		file.createNewFile();
		FileWriter fileW = new FileWriter(file.getAbsoluteFile(), true);
		BufferedWriter fileBw = new BufferedWriter(fileW);

		fileBw.write(texto);
		fileBw.close();
		read.close();
	}

	private boolean leer(File file) throws IOException
	{
		String linea, texto = "";
		file.createNewFile();
		FileReader fileR = new FileReader(file);
		BufferedReader fileBr = new BufferedReader(fileR);

		while ((linea = fileBr.readLine()) != null) {
			texto += linea;
			if (comprobar(texto))
				return true;
		}
		fileBr.close();
		return false;	
	}

	private boolean comprobar(String texto)
	{
		String name_user = texto.substring(0, texto.indexOf(':'));
		String pasw = texto.substring(texto.indexOf(':')+1);
		System.out.println(pasw);

		if (name_user.equals(user.getText()) && pasw.equals(pass.getText()))
			return true;
		else
			return false;
	}
}
