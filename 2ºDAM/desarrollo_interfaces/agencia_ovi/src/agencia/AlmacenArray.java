package agencia;

import java.util.ArrayList;

// Clase encargada de almacenar y devolver dos arrays

public class AlmacenArray {
	
	// Variables
	private ArrayList<String> nombres_iti;
	private ArrayList<Integer> cant_des;
	
	// Constructor
	
	public AlmacenArray(ArrayList<String> nombres_iti, ArrayList<Integer> cant_des)
	{
		this.nombres_iti = nombres_iti;
		this.cant_des = cant_des;
	}
	
	// Devuelve el array que almacena los nombres de los itinerarios
	
	public ArrayList<String> getNombres_iti()
	{
		return nombres_iti;
	}
	
	// Devuelve el array que almacena la cantidad de destinos
	
	public ArrayList<Integer> getCant_des()
	{
		return cant_des;
	}
}