package agencia;

import java.util.ArrayList;

// Clase encargada de realizar operariones sobre el itinerario

public class Itinerario {
	
	// Varaibles
	private ArrayList<String> array;
	
	// Constructor que asigna el array recibido
	
	public Itinerario(ArrayList<String> array)
	{
		this.array = array;
	}
	
	// Constructor que inicializa el array
	
	public Itinerario()
	{
		array = new ArrayList<>();
	}
	
	// Añadir elemto al array
	
	public void add(String elemento)
	{
		array.add(elemento);
	}

	// Borrar array completo
	
	public void borrar()
	{
		array.clear();
	}
	
	// Calcular el tamaño del array
	
	public int tamanio()
	{
		return array.size();
	}
	
	// Obteber elemento de la posicion dada

	public String posicion(int index)
	{
		return array.get(index);
	}
	
	// Eliminar elemento dada la posicion

	public void eliminar(int index)
	{
		array.remove(index);
	}
	
	// Metodo toString
	
	@Override
	public String toString()
	{
		String res = "";
		
		for (int i = 0; i < array.size(); i++)
			res += array.get(i) + "\n";
		return res;
	}
}