package agencia;

import java.io.*;

// Clase encargada de realizar el control de ficheros

public class FicheroItinerarios {
	
	// Variables 
	private BufferedReader fileBr;
	private BufferedWriter fileBw;
	private boolean lectura;
	private String nombre_iti;
	
	// Construtor
	public FicheroItinerarios()
	{
	}
	
	/* Metodo abrir:
	* 
	* Al  llamar  a  este  método, si  el  fichero  con  el  nombre  indicado  
	* no  existe,  se  creará  con  el contenido vacío. En caso contrario, 
	* se procederá a abrir el fichero para realizar operaciones sobre él
	* 
	* */
	public void abrir(String nombre, boolean lectura) throws IOException
	{
		File file = new File(nombre);
		this.lectura = lectura;

		if (!file.exists())
			file.createNewFile();
		
		else if (lectura) {
			FileReader fileR = new FileReader(file);
			fileBr = new BufferedReader(fileR);
			
		} else {
			FileWriter fileW = new FileWriter(file);
			fileBw = new BufferedWriter(fileW);
		}
	}

	/* Metodo leer:
	* 
	* Al  llamar  a  este  método,  leerá  en  el  fichero  y  
	* devolverá el  itinerario  existente  en  esa posición
	* 
	*/
	public Itinerario leer(Itinerario itinerario) throws IOException
	{
		String cant_des;

		itinerario.add(nombre_iti);
		cant_des = fileBr.readLine();
		itinerario.add(cant_des);

		for (int i = 0; i < Integer.parseInt(cant_des); i++)
			itinerario.add(fileBr.readLine());

		return itinerario;
	}

	/* Metodo escribir:
	 * 
	 * Al  llamar  a  este  método,  leerá  en  el  fichero  
	 * y  devolverá el  itinerario  existente  en  esa posición
	 * 
	 */
	public void escribir(Itinerario itinerario) throws IOException
	{
		for (int i = 0; i < itinerario.tamanio(); i++) {
			fileBw.write(itinerario.posicion(i));
			fileBw.newLine();
		}
	}
	
	/* Metodo cerrar:
	 * 
	 * Al  llamar  a  este  método, se  cerrará  el  fichero  
	 * previamente  abierto.  Si  el  fichero  no  se  encontrara 
	 * abierto, devolverá  IOException indicando que el fichero 
	 * no se encuentra abierto.
	 * 
	 */
	public void cerrar() throws IOException
	{
		if (lectura)
			fileBr.close();
		else
			fileBw.close();
	}
	
	/* Metodo Final:
	 * 
	 * Al llamar a este método, devolverá verdadero cuando se haya 
	 * alcanzado el final del fichero. En caso contrario, devolverá falso
	 * 
	 */
	public boolean Final() throws IOException
	{
		return ((nombre_iti = fileBr.readLine()) != null) ? false : true;
	}
	
	/* Metodo vaciar:
	 * 
	 * Al llamar a este método, se borrará el contenido del fichero indicado.
	 * 
	 */
	public void vaciar() throws IOException
	{
		while (fileBr.readLine() != null)
			fileBw.write("");
	}
}