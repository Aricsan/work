package ficheros_buffered;

import java.io.*;
import java.util.Scanner;

public class Main {
	
	static Scanner read = new Scanner(System.in); // Objeto para leer por teclado

	public static void main(String[] args)
	{
		// Variables
		File fileNombres = new File("nombres_personas");
		ControlFichero fichero = new ControlFichero();		
		int opcion; // Opción elegida por el usuario
		boolean salir = false; // Variable de tipo bool para terminar

		do {
			try {
				menu();
				opcion = read.nextInt();

				switch (opcion) {
				case 1:
					fichero.addPersona(crearPersona());
					System.out.println("Persona añadida");
					break;
				case 2:
					fichero.escribirFichero(fileNombres);
					System.out.println("Personas escritas");
					break;
				case 3:
					System.out.println(fichero.imprimirFichero(fileNombres));
					break;
				case 4:
					fichero.borrarFichero(fileNombres);
					System.out.println("Fichero borrado");
					break;
				case 7:
					System.out
							.println("\t--- Gracias por utilizar nustros servicios ---\n\t\t    --- Hasta Pronto ---");
					salir = true;
					break;
				default:
					System.out.println("Opcion Incorrecta");
				}
			} catch (Exception e) {
				System.out.println("Dato Erroneo");
				read.next(); // Limpiar Buffer de espacios tabulaciones y saltos de linea

			}
		} while (!salir);

		read.close(); // Cerrar objeto Scanner
	}

	// Metodo menu
	public static void menu()
	{
		System.out.println(" Elija una opción\n\t1. Añadir persona\n\t2. Escribir personas\n\t3. Imprimir personas\n\t4. Borrar personas\n\t7. Salir");
	}

	public static Persona crearPersona()
	{
		String nombre, dni;
		int edad;
		
		System.out.println("Inserte el nombre, el DNI y la edad");
		
		nombre = read.next();
		dni = read.next();
		edad = read.nextInt();
		System.out.println(nombre+ dni+ edad);
		Persona per = new Persona(nombre, dni, edad);

		return per;
	}
}
