package ficheros_buffered;

import java.io.*;
import java.util.ArrayList;

public class ControlFichero {

	private ArrayList<Persona> array = new ArrayList<Persona>();

	public ControlFichero()
	{
	}

	public String imprimirFichero(File fichero) throws Exception
	{
		FileReader ficheroFr = new FileReader(fichero);
		BufferedReader ficheroBr = new BufferedReader(ficheroFr);
		String linea = "";
		String persona = "";

		while ((persona = ficheroBr.readLine()) != null)
			linea += persona + "\n";

		ficheroBr.close();
		return linea;
	}

	public void escribirFichero(File fichero) throws Exception
	{
		FileWriter ficheroFw = new FileWriter(fichero); // Podemos escribir a pelo en este fichero con write
		BufferedWriter ficheroBw = new BufferedWriter(ficheroFw); // pero este es un objeto prepaeado para ello, con
																	// metodos para ello

		for (int i = 0; i < array.size(); i++) {
			ficheroBw.write(array.get(i).toString());
			ficheroBw.newLine();
		}
		ficheroBw.close();
	}

	public void borrarFichero(File fichero) throws Exception
	{
		FileWriter ficheroFw = new FileWriter(fichero);
		BufferedWriter ficheroBw = new BufferedWriter(ficheroFw);

		for (int i = 0; i < array.size(); i++) {
			ficheroBw.write(" ");
			ficheroBw.newLine();
		}
		ficheroBw.close();
		array.clear();
	}

	public void addPersona(Persona persona)
	{
		array.add(persona);
	}
}