package ficheros;

import java.io.*;

public class Principal {

	public static void main(String[] args) {

		File carpeta = new File("data");
		File fichero = new File(carpeta, "fichero");
		File fichero2 = new File(carpeta, "ficher2");
		File fichero3 = new File(carpeta, "ficher3");

		if (carpeta.mkdir())
			System.out.println("La carpeta se ha creado correctamente");
		else
			System.out.println("No se ha podido crear la carpeta");
		try {
			fichero.createNewFile();
			fichero2.createNewFile();
			fichero3.createNewFile();
		} catch (IOException e) {
			System.out.println("Error al crear los ficheros" + e.getMessage());
		}
	}
}