package repaso_1;

public class Ejercicio_3 {

	public static void main(String[] args) {
		
		// Creacion del Array
		int numeros[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		
		// Informar al Usuario
		System.out.print("Los numeros Pares son: ");

		// Calcular y Mostrar numeros Pares
		for (int i = 0; i < numeros.length; i++)
			if (numeros[i] % 2 == 0)
				System.out.print(numeros[i] + " ");
	}
}
