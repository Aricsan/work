package repaso_1;

import java.util.Scanner;

public class Ejercicio_4 {

	public static void main(String[] args) {
		
		// Variable tipo Scanner (leer por teclado)
		Scanner read = new Scanner(System.in);

		// Informar al Usuario
		System.out.print("Indique el tamaño de la matriz: ");
		
		// Crear matriz y variable suma, pedir datos al usuario
		int suma = 0;
		int matriz[][] = new int[read.nextInt()][read.nextInt()];
		System.out.println();
		
		// Recorrer la matriz añadiendo numeros aleatorios
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				matriz[i][j] = (int) Math.floor(Math.random() * 9 + 1);
				System.out.print(matriz[i][j] + " ");
				suma += matriz[i][j];
			}
			System.out.println();
		}
		
		// Mostrar resultado suma
		System.out.println("\nLa suma total de todos los numeros de la matriz es: " + suma);
	}
}
