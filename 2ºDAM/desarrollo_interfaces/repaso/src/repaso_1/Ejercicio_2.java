package repaso_1;

public class Ejercicio_2 {
	
	public static void main(String[] args) {
		
		// Creacion de Variables
		String nombre = "Antonio";
		int edad = 24;
		char id_letra = 'F';
		double nota_media = 7.43;
		boolean aprobado = true;
		String decision = aprobado == true ? "Si" : "No";
		
		// Mostrar información 
		System.out.print("Su Nombre es: " + nombre + "\nSu Edad es: " + edad + "\nSu Letra Identificativa es: " + 
		id_letra + "\nSu Nota Media es: " + nota_media + "\n¿Esta Usted Aprobado?: " + decision);
	}
}
