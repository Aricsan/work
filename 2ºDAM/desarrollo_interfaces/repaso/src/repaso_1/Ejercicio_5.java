package repaso_1;

import java.util.Scanner;

public class Ejercicio_5 {

	public static void main(String[] args) {

		// Crear variable numero y tipo Scanner (leer por teclado)
		int numero;
		Scanner read = new Scanner(System.in);
		
		// Pedir al usuario un numero positivo
		System.out.print("Introduce un numero positivo: ");
		numero = read.nextInt();
		
		// Comprobación del numero
		while (numero < 1) {
			System.out.print("EL numero debe de ser positivo: ");
			numero = read.nextInt();
		}
		
		// Llamar a la funcion factorial y mostrar su resultado

		System.out.println("EL factorial es: " + factorial(numero));
	}
	
	// Función que calcula el factorial del número introducido
		public static int factorial(int factor) {

			if (factor < 1)
				return 1;

			return factor * factorial(factor - 1);
		}
}
