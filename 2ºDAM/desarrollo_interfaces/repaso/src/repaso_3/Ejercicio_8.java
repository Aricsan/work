package repaso_3;

import repaso_2.Profesor;

public class Ejercicio_8 {

	public static void main(String[] args) {
		
		// Creación de objetos

		Profesor p_A = new Profesor("X645295P", "Florentino", "Perez Gallardo", 1341, 3, "No");
		Profesor p_B = new Profesor("6451347V", "Francisco", "Jimenez Hernandez", 1870, 5, "SI");
		Profesor p_C = new Profesor("4971352X", "Beatriz", "Martinez Pelayo", 1370, 2, "No");

		Modulo m_A = new Modulo("Programacion", 180, p_A, "No");
		Modulo m_B = new Modulo("Bases de Datos", 150, p_B, "No");
		Modulo m_C = new Modulo("Sistemas Informáticos", 120, p_C, "Si");
		
		// Creación y asignación de arrays de tipo Modulo

		Modulo GradoMedio[] = new Modulo[2];
		GradoMedio[0] = m_A;
		GradoMedio[1] = m_C;

		Modulo GradoSuperior[] = new Modulo[3];
		GradoSuperior[0] = m_A;
		GradoSuperior[1] = m_B;
		GradoSuperior[2] = m_C;
		
		// Creación de objetos de tipo Alumno

		Alumno a = new Alumno("46154927K", "Fernando", "Gollo Jimenez", "13/05/2001", 'H', "No", GradoMedio);
		Alumno b = new Alumno("49571542C", "Laura", "Sanchez Castaño", "24/07/1996", 'M', "Si", GradoSuperior);
		
		// Mostrar información (toString) de alumnos 

		System.out.println(" Alumnos Matriculados Santo Domingo Savio\n" + a.toString() + "\n" + b.toString());
	}

}
