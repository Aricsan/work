package repaso_3;

import java.util.Arrays;

public class Alumno {

	// Variables privadas de la clase Alumno
	private String dni;
	private String nombre;
	private String apellidos;
	private String nacimiento;
	private char sexo;
	private String repetidor;
	
	//Array de objetos de tipo Modulo
	Modulo array_modulos[];
	
	// Constructor de clase
	public Alumno(String dni, String nombre, String apellidos, String nacimiento, char sexo, String repetidor, Modulo[] array_modulos) {
		this.dni = dni;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.nacimiento = nacimiento;
		this.sexo = sexo;
		this.repetidor = repetidor;
		this.array_modulos = array_modulos;
	}

	// Getters y Setters
	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Modulo[] getArray_modulos() {
		return array_modulos;
	}

	public void setArray_modulos(Modulo[] array_modulos) {
		this.array_modulos = array_modulos;
	}

	// ToString
	@Override
	public String toString() {
		return "\n\tAlumno [dni=" + dni + ", nombre=" + nombre + ", apellidos=" + apellidos + ", nacimiento="
				+ nacimiento + ", sexo=" + sexo + ", repetidor=" + repetidor + "]\n\tModulos: " + Arrays.toString(array_modulos);
	}
}
