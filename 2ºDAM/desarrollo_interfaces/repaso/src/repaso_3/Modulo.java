package repaso_3;

// Importar clase Profesor
import repaso_2.Profesor;

public class Modulo {

	// Variables privadas de la clase Modulo
	private String nombre;
	private int horas;
	private String convalidable;
	
	// Declarar objeto del tipo Profesor
	Profesor profe;

	public Modulo(String nombre, int horas, Profesor profe, String convalidable) {
		this.nombre = nombre;
		this.horas = horas;
		this.profe = profe;
		this.convalidable = convalidable;
	}
	
	// Getters y Setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getHoras() {
		return horas;
	}

	public void setHoras(int horas) {
		this.horas = horas;
	}

	public String getConvalidable() {
		return convalidable;
	}

	public void setConvalidable(String convalidable) {
		this.convalidable = convalidable;
	}

	// ToString
	@Override
	public String toString() {
		return "nombre Modulo=" + nombre + ", horas=" + horas + " convalidable=" + convalidable + ", profesor: " + profe;
	}
}
