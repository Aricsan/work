package repaso_2;

public class Persona {
	
	// Variables privadas de la clase Persona
	private String dni;
	private String nombre;
	private String apellidos;
	private double salario;

	// Constructor de clase
	public Persona(String dni, String nombre, String apellidos, double salario) {
		this.dni = dni;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.salario = salario;
	}

	// Getters y Setters
	public String getDni() {
		return dni;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public double getSalario() {
		return salario;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	// ToStrig
	@Override
	public String toString() {
		return "dni=" + dni + ", nombre=" + nombre + ", apellidos=" + apellidos + ", salario=" + salario + "€,";
	}
}
