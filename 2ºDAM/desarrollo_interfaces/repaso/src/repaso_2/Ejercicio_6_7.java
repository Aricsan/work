package repaso_2;

public class Ejercicio_6_7 {

	public static void main(String[] args) {
		
		// Creacion de Objetos
		
		Profesor p_a = new Profesor("X645295P", "Florentino", "Perez Gallardo", 1341, 3, "No");
		Profesor p_b = new Profesor("6451347V", "Francisco", "Jimenez Hernandez", 1870, 5, "SI");

		Administracion ad_a = new Administracion("7314657Y", "Mario", "Bross", 1456, "Titulo de Ingeniería Informática", 7);
		Administracion ad_b = new Administracion("T745126O", "Raul", "Castellon Castillo", 1115, "GM Tecnico Informático", 2);

		Directivo d_a = new Directivo("Z645127U", "Alicia", "Pais Maravillas", 2623, "Si", "Manaña");
		Directivo d_b = new Directivo("4951241H", "Laura", "Olivo Gomez", 2476, "No", "Tarde");
		
		// Mostrar información (toString) por pantalla
		
		System.out.println(" Profesores:\n" + p_a.toString() + "\n" + p_b.toString());
		System.out.println(" \n Administrativos:\n" + ad_a.toString() + "\n" + ad_b.toString());
		System.out.println(" \n Directivos:\n" + d_a.toString() + "\n" + d_b.toString());
	}
}
