package repaso_2;

public class Directivo extends Persona {

	// Variables privadas de la clase Directivo
	private String salesiano;
	private String turno;

	// Constructor de clase y clase padre
	public Directivo(String dni, String nombre, String apellidos, int salario, String salesiano, String turno) {
		super(dni, nombre, apellidos, salario);
		this.salesiano = salesiano;
		this.turno = turno;
	}
	
	// Getters y Setters
	public String getSalesiano() {
		return salesiano;
	}

	public String getTurno() {
		return turno;
	}

	public void setSalesiano(String salesiano) {
		this.salesiano = salesiano;
	}

	public void setTurno(String turno) {
		this.turno = turno;
	}

	// ToStrig
	@Override
	public String toString() {
		return super.toString() + " salesiano=" + salesiano + ", turno=" + turno;
	}
}
