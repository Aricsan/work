package repaso_2;

public class Profesor extends Persona {

	// Variables privadas de la clase Profesor
	private int n_asignaturas;
	private String tutor;

	// Constructor de clase y clase padre
	public Profesor(String dni, String nombre, String apellidos, int salario, int n_asignaturas, String tutor) {
		super(dni, nombre, apellidos, salario);
		this.n_asignaturas = n_asignaturas;
		this.tutor = tutor;
	}

	// Getters y Setters
	public int getN_asignaturas() {
		return n_asignaturas;
	}

	public String getTutor() {
		return tutor;
	}

	public void setN_asignaturas(int n_asignaturas) {
		this.n_asignaturas = n_asignaturas;
	}

	public void setTutor(String tutor) {
		this.tutor = tutor;
	}

	// ToStrig
	@Override
	public String toString() {
		return super.toString() + " asignaturas=" + n_asignaturas + ", tutor=" + tutor;
	}
}
