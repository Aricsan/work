package repaso_2;

public class Administracion extends Persona {
	
	// Variables privadas de la clase Administración
	private String estudios;
	private int antiguedad;
	
	// Constructor de clase y clase padre
	public Administracion(String dni, String nombre, String apellidos, int salario, String estudios, int antiguedad) {
		super(dni, nombre, apellidos, salario);
		this.estudios = estudios;
		this.antiguedad = antiguedad;
	}
	
	// Getters y Setters
	public String getEstudios() {
		return estudios;
	}

	public int getAntiguedad() {
		return antiguedad;
	}

	public void setEstudios(String estudios) {
		this.estudios = estudios;
	}

	public void setAntiguedad(int antiguedad) {
		this.antiguedad = antiguedad;
	}
	
	// ToStrig
	@Override
	public String toString() {
		return super.toString() + " estudios=" + estudios + ", antiguedad=" + antiguedad + " años";
	}
}
