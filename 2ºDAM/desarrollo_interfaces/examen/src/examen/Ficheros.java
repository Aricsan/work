package examen;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Ficheros {

	private File usuarios;
	private FileReader fileR;
	private FileWriter fileW;
	private BufferedReader fileBr;
	private BufferedWriter fileBw;
	private boolean lectura;

	public Ficheros(String nombre)
	{
		usuarios = new File(nombre);
	}

	public void abrir(boolean lectura) throws IOException
	{
		if (lectura) {
			this.lectura = true;
			fileR = new FileReader(usuarios);
			fileBr = new BufferedReader(fileR);
		} else {
			this.lectura = false;
			fileW = new FileWriter(usuarios.getAbsoluteFile(), true); // el true si que concatena
			fileBw = new BufferedWriter(fileW);
		}
	}

	public String leer() throws IOException
	{
		String buscar;
		String linea = "";

		/*if ((buscar = fileBr.readLine()) != null)
			linea += buscar + "\n";*/
		linea = fileBr.readLine();

		return linea;
	}

	public void cerrar() throws IOException
	{
		if (lectura)
			fileBr.close();
		else
			fileBw.close();
	}

	public void escribir(String user, String passw) throws IOException
	{

		fileBw.write(user + ":" + passw);
		fileBw.newLine();

		fileBw.close();
	}
}