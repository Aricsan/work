package examen;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JLabel;
import java.awt.FlowLayout;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.JProgressBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import javax.swing.JPasswordField;
import javax.swing.JList;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.AbstractListModel;

public class Examen {

	private JFrame frame;
	private JFrame frame2;
	private JTextField texFieldUsuarios;
	private Ficheros fichero = new Ficheros("usuarios");
	private Timer tiempo;
	private JPasswordField passwordField;
	private JTextField textField;
	JProgressBar progressBar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable() {
			public void run()
			{
				try {
					Examen window = new Examen();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Examen()
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
		// caracteristicas de jframe

		frame = new JFrame();
		frame.setTitle("Practica examen"); // poner el titulo a la ventana
		frame.setResizable(false); // evitar que el usuario modifique el tamaño de la ventana	
		frame.setSize(800, 600);
		frame.setLocationRelativeTo(null); // situa la ventana en el centro de la pantalla
		// frame.setBounds(100, 100, 450, 300); //
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // salir al cerrar la ventana
		frame.getContentPane().setLayout(new GridLayout(2, 2, 10, 10));
		frame.getContentPane().setBackground(Color.magenta); // poner el fondo del todo (el frame el fondo)
		frame.setBackground(Color.MAGENTA); // poner de color el borde de la ventana
		
		// caracteristicas jpanel

		// panel 0
		JPanel panel_0 = new JPanel();
		/*
		 * Container contenedor = frame.getContentPane(); contenedor.add(panel);
		 */
		frame.getContentPane().add(panel_0);
		panel_0.setLayout(null); // layout absolute
		panel_0.setBackground(Color.red);

		// panel 1
		JPanel panel_1 = new JPanel();
		frame.getContentPane().add(panel_1);
		panel_1.setBackground(Color.WHITE);
		panel_1.setLayout(null);
		panel_1.setVisible(false);
		
		JList list = new JList();
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				textField.setText(list.getSelectedValue().toString());
			}
		});
		list.setModel(new AbstractListModel() {
			String[] values = new String[] {"pepe", "juan", "carlo"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		list.setBounds(74, 57, 120, 114);
		panel_1.add(list);
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setBounds(225, 69, 114, 19);
		panel_1.add(textField);
		textField.setColumns(10);

		// panel 2
		JPanel panel_2 = new JPanel();
		frame.getContentPane().add(panel_2);
		panel_2.setBackground(Color.gray);
		panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		// label de imagen panel 2
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("imagenes/registro.jpg"));//.getImage().getScaledInstance(panel_2.getHeight(), panel_2.getHeight(), Image.SCALE_DEFAULT));	
		panel_2.add(lblNewLabel);

		// panel 3
		JPanel panel_3 = new JPanel();
		frame.getContentPane().add(panel_3);
		panel_3.setBackground(Color.pink);
		
		// label de imagen panel 3
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon("imagenes/paisaje.jpg"));
		panel_3.add(lblNewLabel_1);
		
		JButton buttonRegistro = new JButton("Registrarse");
		buttonRegistro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tiempo.start();
				
				try {
					registar (texFieldUsuarios.getText(), passwordField.getText());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		buttonRegistro.setSize(buttonRegistro.getPreferredSize());
		buttonRegistro.setBounds(28, 185, 117, 25);
		panel_0.add(buttonRegistro);

		// boton iniciar sesion
		JButton buttonIniciarSesion = new JButton("Iniciar sesión");
		buttonIniciarSesion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tiempo.start();
				try {
					if (iniciarSesion(texFieldUsuarios.getText(), passwordField.getText())) {
					JOptionPane.showMessageDialog(frame, "Bienvenido");
					panel_1.setVisible(true);
					
					}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		buttonIniciarSesion.setLocation(233, 185);
		buttonIniciarSesion.setSize(buttonIniciarSesion.getPreferredSize());
		// buttonIniciarSesion.setBounds(229, 217, 117, 25);
		panel_0.add(buttonIniciarSesion);
		
		texFieldUsuarios = new JTextField();
		texFieldUsuarios.setBounds(132, 60, 114, 19);
		panel_0.add(texFieldUsuarios);
		texFieldUsuarios.setColumns(10);
		
		// labels
		JLabel lblNewLabel_2 = new JLabel("Usuario");
		lblNewLabel_2.setBounds(28, 62, 70, 15);
		panel_0.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Contraseña");
		lblNewLabel_3.setLocation(28, 129);
		lblNewLabel_3.setSize(lblNewLabel_3.getPreferredSize());
		panel_0.add(lblNewLabel_3);
		
		// barra de progreso
		progressBar = new JProgressBar();
		progressBar.setBounds(117, 243, 148, 14);
		progressBar.setValue(20);
		panel_0.add(progressBar);	
		
		passwordField = new JPasswordField();
		passwordField.setBounds(132, 127, 114, 19);
		panel_0.add(passwordField);
		
		tiempo = new Timer (100, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				progressBar.setValue(progressBar.getValue() + 10);
				
				if (progressBar.getValue() == progressBar.getMaximum()) {
					tiempo.stop();
					progressBar.setValue(0);
					JOptionPane.showMessageDialog(frame, "Finalizado");					
				}
				else
					tiempo.restart();
			}
		});
		
		// Menus

		// menuBar del frame
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		// menus en el menuBar
		JMenu mnNewMenu = new JMenu("Acerca de");
		menuBar.add(mnNewMenu);

		JMenu mnNewMenu_3 = new JMenu("Desarrollador");
		mnNewMenu.add(mnNewMenu_3);
		
		JMenuItem mntmPincha = new JMenuItem("Pincha");
		mntmPincha.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(frame, "Ovidiu Marian Aricsan");
			}
		});
		mnNewMenu_3.add(mntmPincha);
		
		JMenu mnNewMenu_4 = new JMenu("Errores");
		mnNewMenu.add(mnNewMenu_4);

		JMenu mnNewMenu_1 = new JMenu("Ayuda");
		menuBar.add(mnNewMenu_1);

		JMenu mnNewMenu_5 = new JMenu("Programa");
		mnNewMenu_1.add(mnNewMenu_5);

		JMenu mnNewMenu_2 = new JMenu("Configuración");
		menuBar.add(mnNewMenu_2);

		JMenu mnNewMenu_6 = new JMenu("Ventana");
		mnNewMenu_2.add(mnNewMenu_6);
	}
	
	public void registar (String user, String passw) throws IOException
	{
		fichero.abrir(false);
		fichero.escribir(user, passw);
		fichero.cerrar();
	}
	
	public boolean iniciarSesion(String user, String passw) throws IOException
	{
		boolean encontrado = false;
		/*while (!encontrado)
		{
			
		}
		String a = fichero.leer();
		String usuario = a.substring(0, a.indexOf(":"));
		String passw = a.sub*/
		String pepe = user + ":" + passw;
		String a = "";
		fichero.abrir(true);
	
		while (!encontrado && (a = fichero.leer()) != null) 
		{
		if (pepe.equals(a))
			encontrado = true;
		}
		
		fichero.cerrar();
		
		return encontrado;
	}
}
