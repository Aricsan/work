package ejercicio_arrays_01;

public class Numero {
	
	// Variables privadas de la clase
	private int numero;

	// Constructor
	public Numero(int numero) {
		this.numero = numero;
	}
	
	// Get
	public int getValor() {
		return numero;
	}

	// Metodo que sustituye a toString
	public String imprimir() {
		return " " + numero;
	}
}