package ejercicio_arrays_01;

import java.util.ArrayList;

public class TablaNumeros {

	// private Numero Tabla[];																	// Sustituido por array dinamico
	private ArrayList<Numero> Tabla = new ArrayList<Numero>();									// Array dinámico que almacena objetos de tipo Numero
	private int posicion;
	// private int longitud;																	// Variable inutilizable

	// Constructor
	public TablaNumeros() {
	}

	// Metodos
	public void borrar() {
		Tabla.remove(Tabla.size() - 1);
	}

	public int getPosicion() {
		return posicion;
	}

	public String imprimir() {
		String cadena = "";
		for (int i = 0; i < Tabla.size(); i++)
			cadena += "Posicion [" + i + "] = " + Tabla.get(i).getValor() + "\n";
		return cadena;
	}

	public void insertarNumero(Numero numero) {
		Tabla.add(numero);
	}

	public Numero mayor() {
		Numero mayor = Tabla.get(0);

		for (int i = 0; i < Tabla.size() - 1; i++)
			if (mayor.getValor() < Tabla.get(i + 1).getValor())
				mayor = Tabla.get(i + 1);
		return mayor;
	}

	public Numero menor() {
		Numero menor = Tabla.get(0);

		for (int i = 0; i < Tabla.size() - 1; i++)
			if (menor.getValor() > Tabla.get(i + 1).getValor())
				menor = Tabla.get(i + 1);
		return menor;
	}

	public void ordenar() {
		Numero aux;
		for (int i = 0; i < Tabla.size() - 1; i++)
			for (int j = 0; j < Tabla.size() - 1; j++)
				if (Tabla.get(j).getValor() > Tabla.get(j + 1).getValor()) {
					aux = Tabla.get(j + 1);
					Tabla.set(j + 1, Tabla.get(j));
					Tabla.set(j, aux);
				}
	}
}
