package ejercicio_arrays_01;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args)
	{

		// Variables
		Scanner read = new Scanner(System.in); // Objeto para leer por teclado
		TablaNumeros tabla = new TablaNumeros(); // Objeto de tipo TablaNumeros
		int opcion; // Opción elegida por el usuario
		boolean salir = false; // Variable de tipo bool para terminar

		do {
			try {
				menu();
				opcion = read.nextInt();

				switch (opcion) {
				case 1:
					System.out.print("Inserte el numero: ");
					Numero numero = new Numero(opcion = read.nextInt()); // Creamos un objeto de tipo Numero por cada
																			// dato int introducido por el usuario
					tabla.insertarNumero(numero);
					break;
				case 2:
					tabla.borrar();
					break;
				case 3:
					System.out.print(tabla.imprimir());
					break;
				case 4:
					System.out.println("El número menor es: " + tabla.menor()
							.getValor()); /*
											 * tabla.menor() devuelve un objeto de tipo Numero, por lo que es posible
											 * utilizar el metodo getValor con el fin de obtener el dato int
											 */
					break;
				case 5:
					System.out.println("El número mayor es: " + tabla.mayor().getValor());
					break;
				case 6:
					tabla.ordenar();
					break;
				case 9:
					System.out
							.println("\t--- Gracias por utilizar nustros servicios ---\n\t\t    --- Hasta Pronto ---");
					salir = true;
					break;
				default:
					System.out.println("Opcion Incorrecta");
				}
			} catch (Exception e) {
				System.out.println("Dato Erroneo");
				read.next(); // Limpiar Buffer de espacios tabulaciones y saltos de linea

			}
		} while (!salir);

		read.close(); // Cerrar objeto Scanner
	}

	// Metodo menu
	public static void menu()
	{
		System.out.println(" Elija una opción\n\t1. Insertar Número\n\t2. Borrar Número\n\t3. Mostar Tabla\n\t"
				+ "4. Número menor de la Tabla\n\t5. Número mayor de la Tabla\n\t6. Ordenar Tabla\n\t9. Salir");
	}
}
