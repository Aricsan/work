package practicarjsopup;

import java.io.IOException;

import javax.lang.model.element.Element;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Principal {

	public static void main(String[] args) throws IOException
	{
		Document documento;
		String web = "url del titulo";
		
		documento = Jsoup.connect(web).get();

		Elements titular = documento.getElementById("id").getElementsByClass("class"); //funciona sin id pero no sin class
		
		System.out.println(titular.html()); 
	}
}