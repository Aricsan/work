package traductor;

import javax.swing.JFrame;

// ventana modal --> una ventana que esta inhabilitada, como los mensajes de error
// frame.setBounds(100, 100, 450, 300); --> los dos primeros numeros es la posici�n de la ventana en pantalla y los dos ultimos el tama�o de la propia ventana
public class Ventana extends JFrame {

	public Ventana(String titulo)
	{
		setTitle(titulo); // titulo de la ventana
		setResizable(false); // caracteristica que impide al usuario ajustar el tama�o de la ventana
		setSize(450, 300); // definir tama�o de ventana ancho x alto
		setLocationRelativeTo(null); // situa la ventana en el centro de la pantalla
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // al cerrar la aplicacion no se queda en segundo plano
		
		Panel panel = new Panel();
		add(panel);
		
	}
}
