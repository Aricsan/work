package traductor;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;

import javax.swing.Action;
import javax.swing.JTextField;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Evento implements Action {

	public String entrada;
	public JTextField salida;

	public Evento(String palIN, JTextField palOUT)
	{
		entrada = palIN;
		salida = palOUT;
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		try {
			traducir(entrada, salida);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// e.getSource(); --> muestra quien a producido el evento

	}

	private void traducir(String entrada, JTextField salida) throws IOException
	{
		Document document;
		String webPage = "https://www.spanishdict.com/traductor/" + entrada;
		document = Jsoup.connect(webPage).get();
		Elements palabra = document.getElementById("quickdef1-es").getElementsByClass("_1btShz4h");
		salida.setText(palabra.get(0).html());
	}

	@Override
	public Object getValue(String key)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void putValue(String key, Object value)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void setEnabled(boolean b)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isEnabled()
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void addPropertyChangeListener(PropertyChangeListener listener)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener listener)
	{
		// TODO Auto-generated method stub

	}

}
