package traductor;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

// el panel es para organizar el contenido de la ventana, por defecto el panel es toda la ventana

public class Panel extends JPanel {

	public Panel()
	{
		setBackground(Color.cyan); // definir color de fondo
		setLayout(null); // posici�n absoluta en todo el layaout
		
		JLabel palabraIN = new JLabel("Palabra a Traducir"); // crear un nuevo label
		//palabra.setBounds(150, 80, 15, 20); --> asignar posicion y tama�o
		palabraIN.setLocation(100, 40); // asignar solo posici�n
		palabraIN.setSize(palabraIN.getPreferredSize()); // ajustar tama�o al texto que ocupa dentro del label
		palabraIN.setForeground(Color.gray); // color del texto
		add(palabraIN); // a�adirlo 
		
		JLabel palabraOUT = new JLabel("Palabra Traducida"); 
		palabraOUT.setLocation(100, 150); 
		palabraOUT.setSize(palabraOUT.getPreferredSize()); 
		palabraOUT.setForeground(Color.gray); 
		add(palabraOUT);  
		
		JTextField palIN = new JTextField();
		palIN.setBounds(260, 40, 120, 20);
		add(palIN);
		
		JTextField palOUT = new JTextField();
		palOUT.setBounds(260, 145, 120, 20);
		palOUT.setEditable(false);
		add(palOUT);
		
		JButton boton = new JButton("Traducir");
		boton.setLocation(100, 90);
		boton.setSize(boton.getPreferredSize());
		add(boton);
		
		Evento evento = new Evento(palIN.getText(), palOUT);
		boton.addActionListener(evento);
}
}
