package traductorGUI;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Main {

	public static void main(String[] args) throws Exception {
		Document document;
		String webPage = "https://www.spanishdict.com/traductor/table";
		document = Jsoup.connect (webPage).get();
		Elements palabra = document.getElementById("quickdef1-es").getElementsByClass("_1btShz4h");
		palabra.get(0).html();
		System.out.println("Palabra " + palabra.get(0).html());
	}

}
