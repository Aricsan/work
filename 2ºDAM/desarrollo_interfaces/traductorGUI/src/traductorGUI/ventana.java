package traductorGUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.JTextField;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import javax.swing.JButton;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.SwingConstants;

public class ventana {

	private JFrame frame;
	private JTextField textField;
	private JButton btnNewButton;
	private JLabel lblNewLabel_1;

	public String traducir(String pal) throws Exception {
		Document document;
		String webPage = "https://www.spanishdict.com/traductor/" + pal;
		document = Jsoup.connect(webPage).get();
		Elements palabra = document.getElementById("quickdef1-es").getElementsByClass("_1btShz4h");
		palabra.get(0).html();
		return palabra.get(0).html();
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ventana window = new ventana();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ventana() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(30, 165, 144, 20);
		frame.getContentPane().add(lblNewLabel);

		textField = new JTextField();
		textField.setBounds(214, 36, 137, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		btnNewButton = new JButton("Traducir");
		btnNewButton.setBounds(214, 112, 137, 23);
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					lblNewLabel.setText("En Ingles: " + traducir (textField.getText()));
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		frame.getContentPane().add(btnNewButton);
		
		lblNewLabel_1 = new JLabel("Inserte lo que desee traducir:");
		lblNewLabel_1.setBounds(30, 39, 174, 14);
		frame.getContentPane().add(lblNewLabel_1);

	}

}
