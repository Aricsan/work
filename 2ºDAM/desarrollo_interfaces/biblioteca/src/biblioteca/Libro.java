package biblioteca;

public class Libro extends Biblioteca implements Prestable {

	private String prestado;

	public Libro(String codigo, String titulo, int anio_publicacion) {
		super(codigo, titulo, anio_publicacion);
		this.prestado = "No";
	}

	public String getPrestado() {
		return prestado;
	}

	public void setPrestado(String prestado) {
		this.prestado = prestado;
	}

	@Override
	public String prestar() {
		if (prestado == "Si")
			return "El libro ya esta prestado";
		else {
			prestado = "Si";
			return prestado;
		}
	}

	@Override
	public String devolver() {
		if (prestado == "No")
			return "No ha sido prestado";
		else {
			prestado = "No";
			return "Devuelto";
		}
	}

	@Override
	public String prestado() {
		return prestado;
	}

}
