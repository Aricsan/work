package biblioteca;

public class Revista extends Biblioteca {

	public Revista(String codigo, String titulo, int anio_publicacion) {
		super(codigo, titulo, anio_publicacion);
	}

	public int getAnio_publicacion() {
		return getAnio_publicacion();
	}

	public String getCodigo() {
		return getCodigo();
	}

	@Override
	public String toString() {
		return super.toString();
	}

}
