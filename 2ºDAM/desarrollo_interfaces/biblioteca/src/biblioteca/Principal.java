package biblioteca;

public class Principal {

	public static void main(String[] args) {

		Libro lib1 = new Libro("STR153", "Don Quijote de la Mancha", 1605);
		Libro lib2 = new Libro("STR725", "Los pilares de la Tierra", 1989);

		Revista rev1 = new Revista("PGQ024", "Coraz�n", 2019);
		Revista rev2 = new Revista("PGQ718", "�HOLA!", 2020);

		System.out.println(" Libros Disponibles");
		System.out.println("\t" + lib1);
		System.out.println("\t" + lib2);

		lib2.prestar();

		System.out.println("\n Libros prestados");
		System.out.println("\tEl libro " + lib1.getTitulo() + " " + lib1.prestado() + " esta prestado");
		System.out.println("\tEl libro " + lib2.getTitulo() + " " + lib2.prestado() + " esta prestado");

		System.out.println("\n Libros devueltos");
		System.out.println("\tEl libro " + lib1.getTitulo() + " " + lib1.devolver());
		System.out.println("\tEl libro " + lib2.getTitulo() + " " + lib2.devolver());

		System.out.println("\n Revistas");
		System.out.println("\t" + rev1);
		System.out.println("\t" + rev2);
	}
}