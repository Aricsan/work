package biblioteca;

public class Biblioteca {

	private String codigo;
	private String titulo;
	private int anio_publicacion;

	public Biblioteca(String codigo, String titulo, int anio_publicacion) {
		this.codigo = codigo;
		this.titulo = titulo;
		this.anio_publicacion = anio_publicacion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getAnio_publicacion() {
		return anio_publicacion;
	}

	public void setAnio_publicacion(int anio_publicacion) {
		this.anio_publicacion = anio_publicacion;
	}

	@Override
	public String toString() {
		return "codigo=" + codigo + ", titulo=" + titulo + ", a�o_publicacion=" + anio_publicacion;
	}

}
