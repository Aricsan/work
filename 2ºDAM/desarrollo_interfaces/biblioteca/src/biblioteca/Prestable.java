package biblioteca;

interface Prestable {

	String prestar();

	String devolver();

	String prestado();
}
