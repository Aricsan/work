﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private int sumar (int num1, int num2)
        {
            return num1 + num2;
        }
        
        private void restar (ref int num1, ref int num2)
        {
            num1++;
            num2++;
        }

        private void division (int num1, int num2, out int resul)
        {
            resul = num1 / num2;
        }
        private void resta_Click(object sender, EventArgs e)
        {
            int num1 = Int32.Parse(textBoxNumero1.Text);
            int num2 = Int32.Parse(textBoxNumero2.Text);

            restar(ref num1, ref num2);
            resul.Text += num1 - num2;
        }

        private void dividir_Click(object sender, EventArgs e)
        {
            int resultado;
            division(Int32.Parse(textBoxNumero1.Text), Int32.Parse(textBoxNumero2.Text), out resultado);
            resul.Text += resultado;
        }

        private void suma_Click(object sender, EventArgs e)
        {         
            resul.Text +=  sumar(Int32.Parse(textBoxNumero1.Text), Int32.Parse(textBoxNumero2.Text));
        }
    }
}